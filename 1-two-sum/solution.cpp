/*
https://leetcode.com/problems/two-sum

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.
*/
#include <iostream>
#include <vector>
#include "../int_vector.h"

using namespace std;

class Solution {
public:
  vector<int> twoSum(vector<int> &nums, int target) {
    if (nums.empty()) return nums;
    for (int i = 0; i < nums.size() - 1; ++i) {
      for (int j = i + 1; j < nums.size(); ++j) {
        if (nums[i] + nums[j] == target) {
          vector<int> res{i,j};
          return res;
        }
      }
    }
  }
};

int main() {
  Solution sol;
  vector<int> v1{2,7,11,15};
  printIntVector(v1);
  cout << " => ";
  printIntVector(sol.twoSum(v1, 9), true);
}


/*
https://leetcode.com/problems/binary-tree-right-side-view/

199. Binary Tree Right Side View

Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you
can see ordered from top to bottom.

Example 1:

Input: root = [1,2,3,null,5,null,4]
Output: [1,3,4]

Example 2:

Input: root = [1,null,3]
Output: [1,3]

Example 3:

Input: root = []
Output: []

Constraints:

The number of nodes in the tree is in the range [0, 100].
-100 <= Node.val <= 100
*/
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

impl Solution {
  pub fn right_side_view(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<i32> {
    let mut levels = HashMap::new();
    fn inorder(node: Option<Rc<RefCell<TreeNode>>>, level: i32, levels: &mut HashMap<i32, i32>) {
      if let Some(node) = node {
        let node = node.as_ref().borrow();
        if let Some(x) = levels.get_mut(&level) {
          *x = node.val;
        } else {
          levels.insert(level, node.val);
        }
        inorder(node.left.clone(), level + 1, levels);
        inorder(node.right.clone(), level + 1, levels);
      }
    }
    inorder(root, 0, &mut levels);
    let mut res = Vec::with_capacity(levels.len());
    for i in 0..levels.len() as i32 {
      res.push(*levels.get(&i).unwrap());
    }
    res
  }
}

/*
https://leetcode.com/problems/palindrome-pairs/

336. Palindrome Pairs

You are given a 0-indexed array of unique strings words.

A palindrome pair is a pair of integers (i, j) such that:

0 <= i, j < words.length,
i != j, and
words[i] + words[j] (the concatenation of the two strings) is a palindrome.
Return an array of all the palindrome pairs of words.

You must write an algorithm with O(sum of words[i].length) runtime complexity.

Example 1:
Input: words = ["abcd","dcba","lls","s","sssll"]
Output: [[0,1],[1,0],[3,2],[2,4]]
Explanation: The palindromes are ["abcddcba","dcbaabcd","slls","llssssll"]

Example 2:
Input: words = ["bat","tab","cat"]
Output: [[0,1],[1,0]]
Explanation: The palindromes are ["battab","tabbat"]

Example 3:
Input: words = ["a",""]
Output: [[0,1],[1,0]]
Explanation: The palindromes are ["a","a"]

Constraints:

1 <= words.length <= 5000
0 <= words[i].length <= 300
words[i] consists of lowercase English letters.
*/
class Solution {
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> result = new ArrayList<>(words.length * 2);
        var root = buildTrie(words);
        for (int i = 0; i < words.length; ++i) {
            result.addAll(search(root, words[i], i));
        }
        return result;
    }

    private Node buildTrie(String[] words) {
        var root = new Node();
        for (int i = 0; i < words.length; ++i) {
            addWord(root, words[i], i);
        }
        return root;
    }

    private void addWord(Node root, String word, int index) {
        var p = root;
        for (int i = word.length() - 1; i >= 0; --i) {
            // abba - abb - ab - a
            if (isPalindromeSubWord(word, i)) p.indexes.add(index);
            p = p.nextOrCreate(word.charAt(i));
        }
        p.last(index);
    }

    private List<List<Integer>> search(Node root, String word, int index) {
        List<List<Integer>> result = new ArrayList<>();
        var p = root;
        int l = word.length();
        for (int j = 0; j < l; ++j) {
            if (p.index >= 0 && p.index != index && isPalindromeSubWord(word, j, l - 1))
                result.add(List.of(index, p.index));
            p = p.next(word.charAt(j));
            if (p == null) return result;
        }
        for (int j : p.indexes) {
            if (index != j) result.add(List.of(index, j));
        }
        return result;
    }

    private static boolean isPalindromeSubWord(String word, int len) {
        return isPalindromeSubWord(word, 0, len);
    }

    private static boolean isPalindromeSubWord(String word, int i, int j) {
        while (i < j) {
            if (word.charAt(i++) != word.charAt(j--)) return false;
        }
        return true;
    }

    private static class Node {
        Node[] children = new Node['z' - 'a' + 1];
        int index = -1;
        List<Integer> indexes = new ArrayList<>();

        Node next(char c) {
            return children[c - 'a'];
        }

        Node nextOrCreate(char c) {
            int i = c - 'a';
            return children[i] != null ? children[i] : (children[i] = new Node());
        }

        void last(int index) {
            indexes.add(index);
            this.index = index;
        }
    }
}


/*
https://leetcode.com/problems/palindrome-pairs/

336. Palindrome Pairs

You are given a 0-indexed array of unique strings words.

A palindrome pair is a pair of integers (i, j) such that:

0 <= i, j < words.length,
i != j, and
words[i] + words[j] (the concatenation of the two strings) is a palindrome.
Return an array of all the palindrome pairs of words.

You must write an algorithm with O(sum of words[i].length) runtime complexity.

Example 1:
Input: words = ["abcd","dcba","lls","s","sssll"]
Output: [[0,1],[1,0],[3,2],[2,4]]
Explanation: The palindromes are ["abcddcba","dcbaabcd","slls","llssssll"]

Example 2:
Input: words = ["bat","tab","cat"]
Output: [[0,1],[1,0]]
Explanation: The palindromes are ["battab","tabbat"]

Example 3:
Input: words = ["a",""]
Output: [[0,1],[1,0]]
Explanation: The palindromes are ["a","a"]

Constraints:

1 <= words.length <= 5000
0 <= words[i].length <= 300
words[i] consists of lowercase English letters.
*/
class Solution {
    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> result = new ArrayList<>();
        int n = words.length;
        int[] chksums = new int[n];
        char[][] arrays = new char[n][];
        for (int i = 0; i < n; ++i) {
            chksums[i] = 0;
            arrays[i] = words[i].toCharArray();
            for (char c : arrays[i]) {
                chksums[i] ^= 1 << (c - 'a');
            }
        }

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i == j) continue;
                int r = chksums[i] ^ chksums[j];
                if (r != 0 && ((r & (r - 1)) != 0)) continue;
                if (isPalindrome(new ArrayConcatenation(arrays[i], arrays[j])))
                    result.add(List.of(i, j));
            }
        }
        return result;
    }

    private static boolean isPalindrome(ArrayConcatenation word) {
        int i = 0;
        int j = word.length() - 1;
        while (i < j && word.getAt(i) == word.getAt(j)) {
            ++i;
            --j;
        }
        return i >= j;
    }

    private static class ArrayConcatenation {
        private final char[] w1;
        private final int l1;
        private final char[] w2;
        private final int total;

        public ArrayConcatenation(char[] w1, char[] w2) {
            this.w1 = w1;
            this.l1 = w1.length;
            this.w2 = w2;

            this.total = l1 + w2.length;
        }

        public char getAt(int index) {
            return index >= l1 ? w2[index - l1] : w1[index];
        }

        public int length() {
            return total;
        }
    }
}


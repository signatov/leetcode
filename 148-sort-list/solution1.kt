/*
https://leetcode.com/problems/sort-list/

148. Sort List

Given the head of a linked list, return the list after sorting it in ascending order.

Example 1:

Input: head = [4,2,1,3]
Output: [1,2,3,4]

Example 2:

Input: head = [-1,5,3,4,0]
Output: [-1,0,3,4,5]

Example 3:

Input: head = []
Output: []

Constraints:

The number of nodes in the list is in the range [0, 5 * 10^4].
-10^5 <= Node.val <= 10^5

Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?
*/

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    private fun sortListImpl(head: ListNode): ListNode? {
        val array = LinkedListToArrayAdapter(head)

        fun partition(begin: Int, end: Int): Int {
            val pivot = array[end]
            var i = begin - 1

            for (j in begin until end) {
                if (array[j] <= pivot)
                    array.swap(++i, j)
            }

            array.swap(++i, end)

            return i
        }

        fun sort(begin: Int, end: Int) {
            if (begin < end) {
                val index = partition(begin, end)
                sort(begin, index - 1)
                sort(index + 1, end)
            }
        }
        sort(0, array.size - 1)
        return head
    }

    fun sortList(head: ListNode?): ListNode? =
        if (head != null) sortListImpl(head) else null

    data class LinkedListToArrayAdapter(private val head: ListNode) {
        private val indexes: Array<ListNode>
        val size: Int

        init {
            val list = arrayListOf<ListNode>()
            var p: ListNode? = head
            while (p != null) {
                list.add(p)
                p = p.next
            }
            indexes = list.toTypedArray()
            size = indexes.size
        }

        operator fun get(i: Int): Int = indexes[i].`val`

        fun swap(i: Int, j: Int) {
            if (i == j) return
            val ni = indexes[i]
            val nj = indexes[j]
            val t = ni.`val`
            ni.`val` = nj.`val`
            nj.`val` = t
        }
    }
}

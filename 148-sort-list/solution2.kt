/*
https://leetcode.com/problems/sort-list/

148. Sort List

Given the head of a linked list, return the list after sorting it in ascending order.

Example 1:

Input: head = [4,2,1,3]
Output: [1,2,3,4]

Example 2:

Input: head = [-1,5,3,4,0]
Output: [-1,0,3,4,5]

Example 3:

Input: head = []
Output: []

Constraints:

The number of nodes in the list is in the range [0, 5 * 10^4].
-10^5 <= Node.val <= 10^5

Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?
*/

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    fun sortList(head: ListNode?): ListNode? {
        val values = sortedMapOf<Int, Int>()
        var p = head ?: return null
        while (true) {
            values[p.`val`] = values.getOrPut(p.`val`) { 0 }.inc()
            p = p.next ?: break
        }
        val l = LinkedListBuilder()
        values.forEach { (v, c) -> repeat (c) { l.append(v) } }
        return l.get()
    }

    class LinkedListBuilder {
        private var head: ListNode? = null
        private var tail: ListNode? = null

        fun append(v: Int) {
            val n = ListNode(v)
            tail?.next = n
            tail = n
            if (head == null) head = n
        }

        fun get(): ListNode? = head
    }
}

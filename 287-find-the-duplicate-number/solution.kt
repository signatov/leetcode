/*
https://leetcode.com/problems/find-the-duplicate-number/

287. Find the Duplicate Number

Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.

There is only one repeated number in nums, return this repeated number.

You must solve the problem without modifying the array nums and uses only constant extra space.

Example 1:
Input: nums = [1,3,4,2,2]
Output: 2

Example 2:
Input: nums = [3,1,3,4,2]
Output: 3

Constraints:

1 <= n <= 10^5
nums.length == n + 1
1 <= nums[i] <= n
All the integers in nums appear only once except for precisely one integer which appears two or more times.

Follow up:

How can we prove that at least one duplicate number must exist in nums?
Can you solve the problem in linear runtime complexity?
*/
fun Int.checkBit(i: Int): Boolean {
    return (this and (1 shl i)) != 0
}

fun Int.setBit(i: Int): Int {
    return this or (1 shl i)
}

class Solution {
    fun findDuplicate(nums: IntArray): Int {
        var res = 0
        var maxBits = 16  // 10^5 ~ 2*10^16
        val n = nums.size - 1
        while ((n shr maxBits) == 0) maxBits--
        for (i in 0..maxBits) {
            var valuePart = 0
            var indexPart = 0
            for (j in 0..n) {
                if (nums[j].checkBit(i)) valuePart++
                if (j>0 && j.checkBit(i)) indexPart++
            }
            if (valuePart > indexPart) res = res.setBit(i)
        }
        return res
    }
}


/*
https://leetcode.com/problems/text-justification/

68. Text Justification

Given an array of strings words and a width maxWidth, format the text such that each line has exactly maxWidth
characters and is fully (left and right) justified.

You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces
' ' when necessary so that each line has exactly maxWidth characters.

Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line does not
divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.

For the last line of text, it should be left-justified, and no extra space is inserted between words.

Note:

A word is defined as a character sequence consisting of non-space characters only.
Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
The input array words contains at least one word.
*/
class Solution {
    fun fullJustify(words: Array<String>, maxWidth: Int): List<String> {
        val res = mutableListOf<String>()
        val line = mutableListOf<String>()
        for (word in words) {
            if (line.isNotEmpty() && reachLimit(line, word, maxWidth)) {
                res.add(format(line, maxWidth))
                line.clear()
            }
            line.add(word)
        }
        if (line.isNotEmpty()) {
            res.add(format(line, maxWidth, true))
        }
        return res
    }

    private fun reachLimit(words: List<String>, word: String, limit: Int): Boolean {
        return "${words.joinToString(" ")} $word".length > limit
    }

    private fun format(words: List<String>, maxWidth: Int, lastLine: Boolean = false): String {
        if (words.size == 1)
            return words[0].adjust(maxWidth)
        if (lastLine)
            return words.joinToString(" ").adjust(maxWidth)
        val line = words.joinToString("")
        val delta = (maxWidth - line.length) / (words.size - 1)
        var t = (maxWidth - line.length) % (words.size - 1)
        if (t == 0)
            return words.joinToString(" ".repeat(delta))
        var res = ""
        for (i in words.indices) {
            if (i == 0) {
                res = words[0]
                continue
            }
            var d = delta
            if (t > 0) {
                ++d
                --t
            }
            res += " ".repeat(d)
            res += words[i]
        }
        return res
    }

    private fun String.adjust(width: Int): String {
        return if (this.length >= width) this else this + " ".repeat(width - this.length)
    }
}


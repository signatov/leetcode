/*
https://leetcode.com/problems/zigzag-conversion

The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
(you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

And then read line by line: "PAHNAPLSIIGYIR"
Write the code that will take a string and make this conversion given a number of rows:

string convert(string text, int nRows);
convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
*/
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  /*
   *   1                    2n-1                        4n-3
   *   2               2n-2 2n                    4n-4  4n-2
   *   3          2n-3      2n+1            4n-5
   *  ...
   *  ...     n+2                       3n
   *  n-1 n+1               3n-3  3n-1                  5n-5
   *   n                    3n-2                        5n-4
   */
  string convert(string s, int numRows) {
    if (s.empty() || numRows == 1) return s;

    string res(s.size(), 0);
    char * p = &res[0];
    int len = s.size();
    int n = numRows - 1;
    for (int i = 0; i < numRows; ++i) {
      int pos = i;
      if (pos < len) *p++ = s[pos];
      int step1 = (n - i) << 1;
      int step2 = i << 1;
      for (;;) {
        if (step1) {
          if ((pos += step1) >= len) break;
          *p++ = s[pos];
        }
        if (step2) {
          if ((pos += step2) >= len) break;
          *p++ = s[pos];
        }
      }
    }

    return res;
  }
};

int main() {
  Solution sol;
  assert(sol.convert("PAYPALISHIRING", 3) == "PAHNAPLSIIGYIR");
  assert(sol.convert("AB", 1) == "AB");
  cout << sol.convert("vcakvfswgkgszhcscpuhhigcchceedszhujczdaiohapocirreaapicrrfxviruplgtljvtzcvrtxbmqhmrwuvplamjkdfbrlwexsxyusrprubjcncajyqxaeglklobzlhydewzyqprkqcmgfnrdfoflwsvkhylsfjxlhwlxneighwomlfrqwwcupzeyypqfaprinehbjrjqoxezcvsarwvivbgvtybtnuddalgjcpbsofusamtuiocfrldklgebajeaukmzbnrptlhvzcpjsupkhsusyatakxcgjofkezbxmlsmnkvgemqlgmdgnzizwnfidnuhowgajzmwlkepyuchjhnygyxflepotjyhheisfwpqithhqjoztdxbbwioczdwjddshnlnmcduxlbnwrorvntyjdnmdskovpicdvrrxvlvinkegzybmtcywrmbjwpglakqvchvzvshicnqdluqgwqdnceyywglwqetunotigasjqjoddgkzwpoyvoyrumpkqjfdxamgdeptpdysmitixhjtvhmrtcclnjpmjdsmjjhzngrzqnjqwslucxlxbpjoyabkdvyofinuqhvgueyqxjkbjwyklhbmhewmzwbeeqyuxtdrabkxlwausyggghuplscnofrvvsptlsmlwykhvkbpjjxrrrgejkra", 33) << endl;
  assert(sol.convert("ABC", 2) == "ACB");
  assert(sol.convert("ABC", 4) == "ABC");
}

/*
https://leetcode.com/problems/plus-one

Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

You may assume the integer does not contain any leading zero, except the number 0 itself.

Example 1:
Input: [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.

Example 2:
Input: [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
*/
#include <iostream>
#include <vector>
#include "../int_vector.h"

using namespace std;

class Solution {
public:
  vector<int> plusOne(vector<int>& digits) {
    int carry = 0;
    for (int i = digits.size() - 1; i >= 0; --i) {
      ++digits[i];
      carry = digits[i] / 10;
      digits[i] = digits[i] % 10;
      if (!carry) break;
    }
    if (carry) {
      digits.insert(digits.begin(), carry);
    }
    return digits;
  }
};

int main() {
  Solution sol;
  vector<int> v1{9};
  printIntVector(sol.plusOne(v1), true);
  vector<int> v2{1,2,3};
  printIntVector(sol.plusOne(v2), true);
  vector<int> v3{4,3,2,1};
  printIntVector(sol.plusOne(v3), true);
}

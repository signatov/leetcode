/*
Write a function that takes a string as input and returns the string reversed.
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  string reverseString(string s) {
    string res;
    for (string::reverse_iterator r = s.rbegin(); r != s.rend(); ++r) { res += *r; }
    return res;
  }
};

int main() {
  Solution sol;
  cout << "hello => " << sol.reverseString("hello") << endl;
  cout << "pivot => " << sol.reverseString("pivot") << endl;
  cout << "AB => " << sol.reverseString("AB") << endl;
  cout << "AA => " << sol.reverseString("AA") << endl;
}

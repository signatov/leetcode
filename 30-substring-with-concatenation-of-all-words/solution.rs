/*
https://leetcode.com/problems/substring-with-concatenation-of-all-words

30. Substring with Concatenation of All Words

You are given a string s and an array of strings words. All the strings of words are of the same length.

A concatenated string is a string that exactly contains all the strings of any permutation of words concatenated.

For example, if words = ["ab","cd","ef"], then "abcdef", "abefcd", "cdabef", "cdefab", "efabcd", and "efcdab" are all
concatenated strings. "acdbef" is not a concatenated string because it is not the concatenation of any permutation of
words.
Return an array of the starting indices of all the concatenated substrings in s. You can return the answer in any
order.

Example 1:

Input: s = "barfoothefoobarman", words = ["foo","bar"]

Output: [0,9]

Explanation:

The substring starting at 0 is "barfoo". It is the concatenation of ["bar","foo"] which is a permutation of words.
The substring starting at 9 is "foobar". It is the concatenation of ["foo","bar"] which is a permutation of words.

Example 2:

Input: s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]

Output: []

Explanation:

There is no concatenated substring.

Example 3:

Input: s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]

Output: [6,9,12]

Explanation:

The substring starting at 6 is "foobarthe". It is the concatenation of ["foo","bar","the"].
The substring starting at 9 is "barthefoo". It is the concatenation of ["bar","the","foo"].
The substring starting at 12 is "thefoobar". It is the concatenation of ["the","foo","bar"].

Constraints:

1 <= s.length <= 10^4
1 <= words.length <= 5000
1 <= words[i].length <= 30
s and words[i] consist of lowercase English letters.
*/
use std::collections::HashMap;

impl Solution {
  pub fn find_substring(s: String, words: Vec<String>) -> Vec<i32> {
    let l = words[0].len();
    let count = words.len();

    if s.len() < l * count {
      return vec!();
    }

    let mut counters = HashMap::new();
    for w in words {
      let counter = counters.entry(w).or_insert(0);
      *counter += 1;
    }

    let mut res = vec![];

    for i in 0..(s.len() - l * count + 1) {
      let mut j = i;
      let mut c = counters.clone();
      'a: while j < s.len() {
        if j + l > s.len() { break; }
        let ss = &s[j..j+l];
        match c.get_mut(ss) {
          Some(v) => {
            *v -= 1;
            if *v == 0 {
              c.remove(ss);
              if c.is_empty() {
                res.push(i as i32);
                break 'a;
              }
            }
          },
          None => break 'a,
        }
        j = j + l;
      }
    }

    return res;
  }
}


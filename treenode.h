#ifndef __TREE_NODE_H__
#define __TREE_NODE_H__

#include <iostream>
#include <iomanip>

struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  explicit TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void deleteBST(TreeNode *root) {
  if (!root) return;
  deleteBST(root->left);
  deleteBST(root->right);
  delete root;
}

void displayBST(TreeNode *root, int indent = 0) {
  if (!root) return;
  displayBST(root->left, indent + 4);
  if (indent > 0)
    std::cout << std::setw(indent) << " ";
  std::cout << root->val << std::endl;
  displayBST(root->right, indent + 4);
}

#endif // __TREE_NODE_H__


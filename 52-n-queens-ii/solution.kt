/*
https://leetcode.com/problems/n-queens-ii/

52. N-Queens II

The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each
other.

Given an integer n, return the number of distinct solutions to the n-queens puzzle.

Example 1:

.Q..   ..Q.
...Q   Q...
Q...   ...Q
..Q.   .Q..

Input: n = 4
Output: 2
Explanation: There are two distinct solutions to the 4-queens puzzle as shown.

Example 2:

Input: n = 1
Output: 1

Constraints:

1 <= n <= 9
*/
class Solution {
    fun totalNQueens(n: Int): Int {
        val res = mutableListOf<Array<Int>>()
        val board = Array(n) { -1 }

        fun dfs(row: Int = 0) {
            if (row == n) {
                res.add(board)
                return
            }
            for (i in 0 until n) {
                board[row] = i
                if (valid(board, row))
                    dfs(row + 1)
                board[row] = -1
            }
        }

        dfs()
        return res.size
    }

    private fun valid(board: Array<Int>, row: Int): Boolean {
        val y = row
        val x = board[y]
        for (i in 0 until y) {
            val j = board[i]
            val dx = abs(x - j)
            val dy = y - i
            if (dx == 0 || dx == dy)
                return false
        }
        return true
    }
}

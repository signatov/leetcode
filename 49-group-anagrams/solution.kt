/*
https://leetcode.com/problems/group-anagrams/

49. Group Anagrams

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all
the original letters exactly once.

Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Example 2:

Input: strs = [""]
Output: [[""]]

Example 3:

Input: strs = ["a"]
Output: [["a"]]

Constraints:

1 <= strs.length <= 10^4
0 <= strs[i].length <= 100
strs[i] consists of lowercase English letters.
*/
class Solution {
    fun groupAnagrams(strs: Array<String>): List<List<String>> {
        if (strs.isEmpty()) return listOf()
        val m = mutableMapOf<WordInfo, MutableList<String>>()
        for (s in strs) {
            val w = generateInfo(s)
            m.getOrPut(w) { mutableListOf() }.add(s)
        }
        return m.values.map { it as List<String> }
    }

    private fun generateInfo(s: String): WordInfo {
        val size = s.length
        val vocabulary = IntArray('z' - 'a' + 1)
        for (c in s) ++vocabulary[c - 'a']
        return WordInfo(size, vocabulary)
    }

    data class WordInfo(val size: Int, val vocabulary: IntArray) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as WordInfo

            if (size != other.size) return false
            if (!vocabulary.contentEquals(other.vocabulary)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = size
            result = 31 * result + vocabulary.contentHashCode()
            return result
        }
    }
}

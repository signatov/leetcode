/*
https://leetcode.com/problems/string-to-integer-atoi

Implement atoi to convert a string to an integer.

Hint: Carefully consider all possible input cases. If you want a challenge, please do not see below and
ask yourself what are the possible input cases.

Notes: It is intended for this problem to be specified vaguely (ie, no given input specs). You are
responsible to gather all the input requirements up front.

 

Requirements for atoi:

The function first discards as many whitespace characters as necessary until the first non-whitespace
character is found. Then, starting from this character, takes an optional initial plus or minus sign followed
by as many numerical digits as possible, and interprets them as a numerical value.

The string can contain additional characters after those that form the integral number, which are
ignored and have no effect on the behavior of this function.

If the first sequence of non-whitespace characters in str is not a valid integral number, or if no such
sequence exists because either str is empty or it contains only whitespace characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned. If the correct value is out of the range
of representable values, INT_MAX (2147483647) or INT_MIN (-2147483648) is returned.
*/
#include <cassert>
#include <cctype>
#include <climits>
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  int myAtoi(string str) {
    if (str.empty() || str == "") return 0;
    const char * p = str.c_str();
    while (*p && isspace(*p)) ++p;
    if (!*p) return 0;
    long long res = 0;
    int sign = 1;
    if (*p == '+') ++p;
    else if (*p == '-') sign = -1, ++p;
    for (; *p; ++p) {
      if (!isdigit(*p)) break;
      res = res * 10 + (*p - '0');
      if (sign * res <= INT_MIN) return INT_MIN;
      if (sign * res >= INT_MAX) return INT_MAX;
    }
    res *= sign;
    return res <= INT_MIN ? INT_MIN : res >= INT_MAX ? INT_MAX : (int)res;
  }
};

int main() {
  Solution sol;
  assert(sol.myAtoi("0") == 0);
  assert(sol.myAtoi("1") == 1);
  assert(sol.myAtoi("-1") == -1);
  assert(sol.myAtoi("   +123") == 123);
  assert(sol.myAtoi(" -666") == -666);
  assert(sol.myAtoi("45324523452345") == 0);
  assert(sol.myAtoi("2147483647") == 2147483647);
}


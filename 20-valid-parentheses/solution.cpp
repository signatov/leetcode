/*
https://leetcode.com/problems/valid-parentheses

Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

* Open brackets must be closed by the same type of brackets.
* Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:

Input: "()"
Output: true

Example 2:

Input: "()[]{}"
Output: true

Example 3:

Input: "(]"
Output: false

Example 4:

Input: "([)]"
Output: false

Example 5:

Input: "{[]}"
Output: true
*/
#include <cassert>
#include <iostream>
#include <stack>
#include <string>

using namespace std;

class Solution {
  enum class BktType { Round, Square, Brace };

  static bool isBracket(const char ch) {
    return ch == '(' || ch == ')' ||
           ch == '[' || ch == ']' ||
           ch == '{' || ch == '}';
  }

  static bool isClosingBracket(const char ch) {
    return ch == ')' || ch == ']' || ch == '}';
  }

  static BktType getBracketType(const char ch) {
    switch (ch) {
      case '(': case ')': return BktType::Round;
      case '[': case ']': return BktType::Square;
      case '{': case '}': return BktType::Brace;
    }
  }

public:
  bool isValid(string s) {
    std::stack<BktType> bkt;
    for (int i = 0; i < s.size(); ++i) {
      if (isBracket(s[i])) {
        BktType type = getBracketType(s[i]);
        if (isClosingBracket(s[i])) {
          if (bkt.empty() || bkt.top() != type) {
            return false;
          }
          bkt.pop();
        } else {
          bkt.push(type);
        }
      }
    }
    return bkt.empty();
  }
};

int main() {
  Solution sol;
  assert(sol.isValid("()"));
  assert(sol.isValid("()[]{}"));
  assert(!sol.isValid("(]"));
  assert(!sol.isValid("([)]"));
  assert(sol.isValid("{[]}"));
}

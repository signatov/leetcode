/*
https://leetcode.com/problems/minimum-window-substring/

76. Minimum Window Substring

Given two strings s and t of lengths m and n respectively, return the minimum window substring of s such that every
character in t (including duplicates) is included in the window. If there is no such substring, return the empty string
"".

The testcases will be generated such that the answer is unique.

Example 1:

Input: s = "ADOBECODEBANC", t = "ABC"
Output: "BANC"
Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.

Example 2:

Input: s = "a", t = "a"
Output: "a"
Explanation: The entire string s is the minimum window.

Example 3:

Input: s = "a", t = "aa"
Output: ""
Explanation: Both 'a's from t must be included in the window.
Since the largest window of s only has one 'a', return empty string.

Constraints:

m == s.length
n == t.length
1 <= m, n <= 10^5
s and t consist of uppercase and lowercase English letters.

Follow up: Could you find an algorithm that runs in O(m + n) time?
*/
use std::convert::From;

const DICT_SIZE: usize = 128;
struct Dict([i32; DICT_SIZE]);

impl From<&String> for Dict {
  fn from(s: &String) -> Self {
    let mut a: [i32; DICT_SIZE] = [0; DICT_SIZE];
    s.bytes().for_each(|c| a[c as usize] += 1);
    Self(a)
  }
}

impl Dict {
  fn inc(&mut self, c: u8) -> i32 {
    let i = c as usize;
    self.0[i] += 1;
    self.0[i]
  }

  fn dec(&mut self, c: u8) -> i32 {
    let i = c as usize;
    let old = self.0[i];
    self.0[i] -= 1;
    old
  }
}

impl Solution {
  pub fn min_window(s: String, t: String) -> String {
    if s.len() < t.len() { return String::from(""); }
    let len = s.len();
    let chars = s.as_bytes();
    let mut ds = Dict::from(&t);
    let mut start = 0;
    let mut end = 0;
    let mut min_window = usize::MAX;
    let mut min_window_pos = 0;
    let mut counter = t.len() as i32;
    while end < len {
      let c = chars[end];
      end += 1;
      counter -= if ds.dec(c) > 0 { 1 } else { 0 };

      while counter == 0 {
        let curr_window = end - start;
        if min_window > curr_window {
          min_window = curr_window;
          min_window_pos = start;
        }

        let c = chars[start];
        start += 1;
        counter += if ds.inc(c) > 0 { 1 } else { 0 };
      }
    }
    if min_window == usize::MAX { String::from("") }
    else { s[min_window_pos..min_window_pos + min_window].into() }
  }
}

/*
https://leetcode.com/problems/merge-intervals/
Given an array of intervals where intervals[i] = [starti, endi], merge all
overlapping intervals, and return an array of the non-overlapping intervals
that cover all the intervals in the input.

Example 1:
Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].

Example 2:
Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.

Constraints:
1 <= intervals.length <= 104
intervals[i].length == 2
0 <= starti <= endi <= 104
*/

#include <algorithm>
#include <cassert>
#include <vector>
using std::vector;

class Solution {
  static bool comparator(const vector<int>& a, const vector<int>& b) {
    if (a[0] < b[0]) return true;
    if (b[0] < a[0]) return false;
    return a[1] < b[1];
  }
  static bool same(const vector<int>& a, const vector<int>& b) {
    return a[0] == b[0] && a[1] == b[1];
  }
public:
  vector<vector<int>> merge(vector<vector<int>>& intervals) {
    vector<vector<int>> res;
    res.reserve(intervals.size());
    std::sort(intervals.begin(), intervals.end(), comparator);
    for (auto i = intervals.begin(); i < intervals.end(); ++i) {
      if (res.empty()) {
        res.push_back(*i);
        continue;
      }

      vector<vector<int>>::reference r = res.back();
      if (same(*i, r)) {
        continue;
      }

      int left = i->at(0);
      int right = i->at(1);
      if (r[1] < left) {
        res.push_back(*i);
        continue;
      }

      if (right > r[1]) {
        r[1] = right;
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  vector<vector<int>> intervals1 = {{1,3}, {2,6}, {8,10}, {15,18}};
  auto res = sol.merge(intervals1);
  assert(res.size() == 3);
  assert(res[0][0] == 1);
  assert(res[0][1] == 6);
}

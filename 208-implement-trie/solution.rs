/*
https://leetcode.com/problems/implement-trie-prefix-tree

208. Implement Trie (Prefix Tree)

A trie (pronounced as "try") or prefix tree is a tree data structure used to efficiently store and retrieve keys in a
dataset of strings. There are various applications of this data structure, such as autocomplete and spellchecker.

Implement the Trie class:

Trie() Initializes the trie object.
- void insert(String word) Inserts the string word into the trie.
- boolean search(String word) Returns true if the string word is in the trie (i.e., was inserted before), and false
otherwise.
- boolean startsWith(String prefix) Returns true if there is a previously inserted string word that has the prefix
prefix, and false otherwise.

Example 1:

Input
["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
[[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
Output
[null, null, true, false, true, null, true]

Explanation
Trie trie = new Trie();
trie.insert("apple");
trie.search("apple");   // return True
trie.search("app");     // return False
trie.startsWith("app"); // return True
trie.insert("app");
trie.search("app");     // return True

Constraints:

1 <= word.length, prefix.length <= 2000
word and prefix consist only of lowercase English letters.
At most 3 * 10^4 calls in total will be made to insert, search, and startsWith.
*/
use std::collections::HashMap;

#[derive(Default)]
struct TreeNode {
    children: HashMap<u8, TreeNode>,
    last_char: bool,
}

#[derive(Default)]
struct Trie {
    root: TreeNode,
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl Trie {
    fn new() -> Self {
        Default::default()
    }

    fn insert(&mut self, word: String) {
        let mut curr = &mut self.root;
        for c in word.into_bytes() {
            curr = curr.children.entry(c).or_default();
        }
        curr.last_char = true;
    }

    fn search(&self, word: String) -> bool {
        let mut curr = &self.root;
        for c in word.into_bytes() {
            match curr.children.get(&c) {
                Some(n) => curr = n,
                None => return false,
            }
        }
        curr.last_char
    }

    fn starts_with(&self, prefix: String) -> bool {
        let mut curr = &self.root;
        for c in prefix.into_bytes() {
            match curr.children.get(&c) {
                Some(n) => curr = n,
                None => return false,
            }
        }
        true
    }
}

/**
 * Your Trie object will be instantiated and called as such:
 * let obj = Trie::new();
 * obj.insert(word);
 * let ret_2: bool = obj.search(word);
 * let ret_3: bool = obj.starts_with(prefix);
 */

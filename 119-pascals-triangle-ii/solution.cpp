/*
https://leetcode.com/problems/pascals-triangle-ii

Given a non-negative index k where k ≤ 33, return the kth index row of the Pascal's triangle.

Note that the row index starts from 0.

In Pascal's triangle, each number is the sum of the two numbers directly above it.

Example:
Input: 3
Output: [1,3,3,1]

Follow up:
Could you optimize your algorithm to use only O(k) extra space?
*/
#include <iostream>
#include <vector>
#include "../int_vector.h"

using namespace std;

class Solution {
public:
  vector<int> getRow(int rowIndex) {
    if (rowIndex == 0) return {1};
    if (rowIndex == 1) return {1, 1};
    vector<int> res(rowIndex + 1, 0);
    res[0] = res[1] = 1;
    for (int i = 2; i <= rowIndex; ++i) {
      for (int j = 1, t = 1; j < i; ++j) {
        int z = res[j];
        res[j] += t;
        t = z;
      }
      res[i] = 1;
    }
    return res;
  }
};

int main() {
  Solution sol;
  const vector<int> & row = sol.getRow(5);
  printIntVector(row, true);
}


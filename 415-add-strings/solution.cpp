/*
https://leetcode.com/problems/add-strings
Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
*/
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Solution {
  static const string digits[];
public:
  string addStrings(string num1, string num2) {
    int delta = abs(static_cast<int>(num1.size() - num2.size()));
    if (delta > 0) {
      string padding(delta, '0');
      if (num1.size() < num2.size())
        num1 = padding + num1;
      else
        num2 = padding + num2;
    }

    string res;
    int carry = 0;
    for (int i = num1.size() - 1; i >= 0; --i) {
      int x = (num1[i] - '0') + (num2[i] - '0') + carry;
      int r = x % 10;
      carry = x / 10;
      res = digits[r] + res;
    }

    if (carry > 0)
      res = digits[carry] + res;

    return res;
  }
};

const string Solution::digits[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

int main() {
  Solution sol;
  cout << "12345 + 123 = " << sol.addStrings("12345", "123") << endl;
  cout << "1 + 1 = " << sol.addStrings("1", "1") << endl;
  cout << "45234524352435234532 + 1231231 = " << sol.addStrings("45234524352435234532", "1231231") << endl;
  cout << "3452345 + 56 = " << sol.addStrings("3452345", "56") << endl;
  cout << "1 + 9 = " << sol.addStrings("1", "9") << endl;
  cout << "99 + 1 = " << sol.addStrings("99", "1") << endl;
  cout << "666 + 333 = " << sol.addStrings("666", "333") << endl;
}

/*
https://leetcode.com/problems/simplify-path

Given an absolute path for a file (Unix-style), simplify it.

For example,
path = "/home/", => "/home"
path = "/a/./b/../../c/", => "/c"

Corner Cases:
Did you consider the case where path = "/../"?
In this case, you should return "/".

Another corner case is the path might contain multiple slashes '/' together, such as "/home//foo/".
In this case, you should ignore redundant slashes and return "/home/foo".
*/
#include <deque>
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static void check_name(deque<string> & res, const string & name) {
    if (name.empty() || name == ".") return;
    if (name == "..") {
      if (!res.empty()) res.pop_back();
    }
    else res.push_back(name);
  }
  static deque<string> split_path(const string & path) {
    deque<string> res;
    size_t prev = 0;
    for (size_t next = path.find('/', prev);
         next != string::npos;
         prev = next + 1, next = path.find('/', prev)) {
      string s{&path[prev], next - prev};
      check_name(res, s);
    }
    if (prev < path.size()) {
      check_name(res, string{&path[prev], path.size() - prev});
    }
    return res;
  }
  static string make_path(const deque<string> & parts) {
    if (parts.empty()) return "/";
    string res;
    for (auto i : parts) { res = res + "/" + i; }
    return res;
  }
public:
  string simplifyPath(string path) {
    deque<string> parts = std::move(split_path(path));
    return make_path(parts);
  }
};

int main() {
  Solution sol;
  cout << sol.simplifyPath("/.") << endl;
  cout << sol.simplifyPath("/..") << endl;
  cout << sol.simplifyPath("////home") << endl;
  cout << sol.simplifyPath("////home/////abc") << endl;
  cout << sol.simplifyPath("////home/../home////abc/./bac//../last") << endl;
}

/*
https://leetcode.com/problems/swap-nodes-in-pairs

Given a linked list, swap every two adjacent nodes and return its head.

Example:

Given 1->2->3->4, you should return the list as 2->1->4->3.

Note:

* Your algorithm should use only constant extra space.
* You may not modify the values in the list's nodes, only nodes itself may be changed.
*/
#include <algorithm>
#include <cassert>
#include <iostream>
#include "../listnode.h"

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
  static void swapPair(ListNode *&l, ListNode *&r) {
    assert(l->next == r);
    ListNode *t = r->next;
    r->next = l;
    l->next = t;
    swap(l, r);
  }
public:
  ListNode *swapPairs(ListNode *head) {
    if (!head || !head->next) return head;
    ListNode *h = head, *n = head->next;
    head = head->next;
    ListNode *p = NULL;
    while (h && n) {
      ListNode *t = n->next;
      ListNode *v = t ? t->next : NULL;
      swapPair(h, n);
      if (p) p->next = h;
      p = n;
      h = t;
      n = v;
    }
    return head;
  }
};

void check1() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{1,2,3,4}), &deleteList};
  ListNode *n = sol.swapPairs(l1.get());
  printList(n);
  assert(n->val == 2);
  assert(n->next->val == 1);
  assert(n->next->next->val == 4);
  assert(n->next->next->next->val == 3);
}

void check2() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{1,2,3}), &deleteList};
  ListNode *n = sol.swapPairs(l1.get());
  printList(n);
  assert(n->val == 2);
  assert(n->next->val == 1);
  assert(n->next->next->val == 3);
}

int main() {
  check1();
  check2();
}


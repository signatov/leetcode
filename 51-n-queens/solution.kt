/*
https://leetcode.com/problems/n-queens/

51. N-Queens

The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each
other.

Given an integer n, return all distinct solutions to the n-queens puzzle. You may return the answer in any order.

Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a
queen and an empty space, respectively.

Example 1:

.Q..   ..Q.
...Q   Q...
Q...   ...Q
..Q.   .Q..

Input: n = 4
Output: [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
Explanation: There exist two distinct solutions to the 4-queens puzzle as shown above

Example 2:

Input: n = 1
Output: [["Q"]]

Constraints:

1 <= n <= 9
*/
class Solution {
    fun solveNQueens(n: Int): List<List<String>> {
        val res = mutableListOf<List<String>>()
        val board = Array(n) { -1 }

        fun dfs(row: Int = 0) {
            if (row == n) {
                res.add(board.convert())
                return
            }
            for (i in 0 until n) {
                board[row] = i
                if (valid(board, row))
                    dfs(row + 1)
                board[row] = -1
            }
        }

        dfs()
        return res
    }

    private fun valid(board: Array<Int>, row: Int): Boolean {
        val y = row
        val x = board[y]
        for (i in 0 until y) {
            val j = board[i]
            val dx = abs(x - j)
            val dy = y - i
            if (dx == 0 || dx == dy)
                return false
        }
        return true
    }

    private fun Array<Int>.convert(): List<String> {
        val res = mutableListOf<String>()
        for (x in this) {
            if (x == -1) break
            val sb = StringBuilder()
            for (j in 0 until size)
              sb.append(if (j == x) 'Q' else '.')
            res.add(sb.toString())
        }
        return res
    }
}

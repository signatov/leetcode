/*
https://leetcode.com/problems/coin-change/

322. Coin Change

You are given an integer array coins representing coins of different denominations and an integer amount representing
a total amount of money.

Return the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by
any combination of the coins, return -1.

You may assume that you have an infinite number of each kind of coin.

Example 1:

Input: coins = [1,2,5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1

Example 2:

Input: coins = [2], amount = 3
Output: -1

Example 3:

Input: coins = [1], amount = 0
Output: 0

Constraints:

1 <= coins.length <= 12
1 <= coins[i] <= 2^31 - 1
0 <= amount <= 10^4
*/
class Solution {
    fun coinChange(coins: IntArray, amount: Int): Int {
        if (amount == 0) return 0
        val dp = arrayOfNulls<Int>(amount + 1)
        dp[0] = 0
        dp[amount] = -1
        for (v in 1..amount) {
            var r = Int.MAX_VALUE
            for (c in coins) {
                val d = v - c
                if (d >= 0) {
                    dp[d]?.let {
                        r = min(r, 1 + it)
                    }
                }
            }
            if (r in 1..amount)
                dp[v] = r
        }
        return dp[amount]!!
    }
}

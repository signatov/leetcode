/*
https://leetcode.com/problems/reverse-linked-list-ii/

92. Reverse Linked List II

Given the head of a singly linked list and two integers left and right where left <= right, reverse the nodes of the
list from position left to position right, and return the reversed list.

Example 1:

Input: head = [1,2,3,4,5], left = 2, right = 4
Output: [1,4,3,2,5]

Example 2:

Input: head = [5], left = 1, right = 1
Output: [5]

Constraints:

The number of nodes in the list is n.
1 <= n <= 500
-500 <= Node.val <= 500
1 <= left <= right <= n
*/
import java.util.ArrayDeque

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    fun reverseBetween(head: ListNode?, left: Int, right: Int): ListNode? {
        check(head != null)
        var i = 1
        var l = head
        while (i++ < left && l != null)
            l = l.next
        if (l != null) {
            val s = copyElements(l, right - left + 1)
            while (s.isNotEmpty() && l != null) {
                l.`val` = s.pop()
                l = l.next
            }
        }
        return head
    }

    private fun copyElements(p: ListNode?, count: Int): ArrayDeque<Int> {
        check(p != null)
        val res = ArrayDeque<Int>()
        var c = count
        var l = p
        while (l != null && c-- > 0) {
            res.push(l!!.`val`)
            l = l!!.next
        }
        return res
    }
}

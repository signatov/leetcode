package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMergeTwoLists(t *testing.T) {
	l1 := new(ListNode)
	l1.Val = 1
	l1.Next = new(ListNode)
	l1.Next.Val = 2
	l1.Next.Next = new(ListNode)
	l1.Next.Next.Val = 4

	l2 := new(ListNode)
	l2.Val = 1
	l2.Next = new(ListNode)
	l2.Next.Val = 3
	l2.Next.Next = new(ListNode)
	l2.Next.Next.Val = 4

	res := mergeTwoLists(l1, l2)
	assert.Equal(t, res.Val, 1, "Test#1 failed")
	assert.Equal(t, res.Next.Val, 1, "Test#1 failed")
	assert.Equal(t, res.Next.Next.Val, 2, "Test#1 failed")
}

package solution

// https://leetcode.com/problems/merge-two-sorted-lists

// Merge two sorted linked lists and return it as a new list.
// The new list should be made by splicing together the nodes of the first two lists.
// Example:
// Input: 1->2->4, 1->3->4
// Output: 1->1->2->3->4->4

// ListNode declaration
type ListNode struct {
	Val  int
	Next *ListNode
}

func appendNode(head *ListNode, tail *ListNode, val int) (*ListNode, *ListNode) {
	if tail == nil {
		tail = new(ListNode)
		head = tail
		head.Val = val
	} else {
		x := new(ListNode)
		x.Val = val
		tail.Next = x
		tail = x
	}
	return head, tail
}

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	var head *ListNode
	var tail *ListNode
	i := l1
	j := l2
	var t int
	for i != nil && j != nil {
		if i.Val < j.Val {
			t = i.Val
			i = i.Next
		} else {
			t = j.Val
			j = j.Next
		}
		head, tail = appendNode(head, tail, t)
	}

	if j != nil {
		i = j
	}
	for ; i != nil; i = i.Next {
		head, tail = appendNode(head, tail, i.Val)
	}

	return head
}

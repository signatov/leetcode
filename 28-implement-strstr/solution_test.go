package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStrStr(t *testing.T) {
	assert.Equal(t, strStr("abc", "hi"), -1, "Test#1 failed")
	assert.Equal(t, strStr("hello", "ll"), 2, "Test#1 failed")
	assert.Equal(t, strStr("hello", ""), 0, "Test#1 failed")
}

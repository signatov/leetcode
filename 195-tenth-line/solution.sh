#!/bin/bash

# https://leetcode.com/problems/tenth-line
# How would you print just the 10th line of a file?

# Read from the file file.txt and output the tenth line to stdout.
cat file.txt | while read -r line1; do
  read -r line2
  read -r line3
  read -r line4
  read -r line5
  read -r line6
  read -r line7
  read -r line8
  read -r line9
  if read -r line10; then
    echo $line10
  fi
  exit
done

/*
International Morse Code defines a standard encoding where each letter is mapped to a series of dots and dashes, as follows:
"a" maps to ".-", "b" maps to "-...", "c" maps to "-.-.", and so on.

For convenience, the full table for the 26 letters of the English alphabet is given below:

[".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
Now, given a list of words, each word can be written as a concatenation of the Morse code of each letter.
For example, "cab" can be written as "-.-.-....-", (which is the concatenation "-.-." + "-..." + ".-"). We'll call such a concatenation, the transformation of a word.

Return the number of different transformations among all words we have.
*/
#include <iostream>
#include <array>
#include <map>
#include <vector>
#include <string>

using namespace std;

static const array<string, 26> transformations{
  ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."
};

class Solution {
public:
  int uniqueMorseRepresentation(vector<string>& words) {
    map<string, int> uniqueTransformations;
    for (auto word : words) {
      string transformed;
      for (auto ch : word) {
        transformed += transformations[ch - 'a'];
      }
      int count = uniqueTransformations[transformed];
      ++count;
      uniqueTransformations[transformed] = count;
    }
    return uniqueTransformations.size();
  }
};

int main() {
  Solution sol;
  vector<string> input{"gin", "zen", "gig", "msg"};
  cout << sol.uniqueMorseRepresentation(input) << endl;
}


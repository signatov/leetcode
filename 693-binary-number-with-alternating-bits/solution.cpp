/*
Given a positive integer, check whether it has alternating bits: namely, if two adjacent bits will always have different values.
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static bool has_bit(int n, int i) {
    return ((unsigned)n & (1 << i)) != 0;
  }
  static int max_set_bit(int n) {
    for (int i = 31; i >= 0; --i) {
      if (n & (1 << i)) return i;
    }
    return 0;
  }
public:
  bool hasAlternatingBits(int n) {
    for (int i = 0; i <= max_set_bit(n) - 1; ++i) {
      bool b1 = has_bit(n, i + 0);
      bool b2 = has_bit(n, i + 1);
      if (b1 == b2) return false;
    }
    return true;
  }
};

int main() {
  Solution sol;
  cout << sol.hasAlternatingBits(5) << endl;
  cout << sol.hasAlternatingBits(7) << endl;
  cout << sol.hasAlternatingBits(11) << endl;
  cout << sol.hasAlternatingBits(10) << endl;
}

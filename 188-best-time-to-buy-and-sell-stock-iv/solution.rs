/*
https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/

188. Best Time to Buy and Sell Stock IV

You are given an integer array prices where prices[i] is the price of a given stock on the ith day, and an integer k.

Find the maximum profit you can achieve. You may complete at most k transactions: i.e. you may buy at most k times and
sell at most k times.

Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).

Example 1:

Input: k = 2, prices = [2,4,1]
Output: 2
Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.

Example 2:

Input: k = 2, prices = [3,2,6,5,0,3]
Output: 7
Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4. Then buy on day 5 (price = 0)
and sell on day 6 (price = 3), profit = 3-0 = 3.

Constraints:

1 <= k <= 100
1 <= prices.length <= 1000
0 <= prices[i] <= 1000
*/
#[derive(Copy, Clone)]
struct Transaction {
  buy: i32,
  profit: i32
}

impl Default for Transaction {
  fn default() -> Self {
      Transaction { buy: i32::MIN, profit: 0 }
  }
}

impl Transaction {
  fn update(&mut self, prev: Transaction, price: i32) {
    self.buy = std::cmp::max(self.buy, prev.profit - price);
    self.profit = std::cmp::max(self.profit, self.buy + price);
  }
}

impl Solution {
  pub fn max_profit(k: i32, prices: Vec<i32>) -> i32 {
    let mut txs = vec![Transaction::default(); (k + 1) as usize];
    for price in prices {
      for i in 1..txs.len() {
        let prev = txs[i - 1].clone();
        txs[i].update(prev, price);
      }
    }
    txs[txs.len() - 1].profit
  }
}

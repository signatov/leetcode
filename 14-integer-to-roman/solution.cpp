/*
https://leetcode.com/problems/integer-to-roman

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

For example, two is written as II in Roman numeral, just two one's added together. 
Twelve is written as, XII, which is simply X + II. The number twenty seven is written as XXVII,
which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for
four is not IIII. Instead, the number four is written as IV. Because the one is before the five we
subtract it making four. The same principle applies to the number nine, which is written as IX.
There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.

Given an integer, convert it to a roman numeral. Input is guaranteed to be within the range from 1 to 3999.

Example 1:
Input: 3
Output: "III"

Example 2:
Input: 4
Output: "IV"

Example 3:
Input: 9
Output: "IX"

Example 4:
Input: 58
Output: "LVIII"
Explanation: C = 100, L = 50, XXX = 30 and III = 3.

Example 5:
Input: 1994
Output: "MCMXCIV"
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
*/
#include <cassert>
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static string parse(int unit, char one, char half, char ten) {
    switch (unit) {
      case 0: return "";
      case 1:
      case 2:
      case 3:
        return string(unit, one);
      case 4: return string(1, one) + string(1, half);
      case 5: return string(1, half);
      case 6:
      case 7:
      case 8:
        return string(1, half) + string(unit - 5, one);
      case 9: return string(1, one) + string(1, ten);
      default: break;
    }
    assert(!"Unknown unit");
  }
  static string parseUnits(int unit) {
    return parse(unit, 'I', 'V', 'X');
  }
  static string parseDozens(int unit) {
    return parse(unit, 'X', 'L', 'C');
  }
  static string parseHundreds(int unit) {
    return parse(unit, 'C', 'D', 'M');
  }
  static string parseThousands(int unit) {
    return string(unit, 'M');
  }
public:
  string intToRoman(int num) {
    assert(num >= 1 && num <= 3999);
    int thousands = num / 1000;
    int hundreds = (num - thousands * 1000) / 100;
    int dozens = (num - thousands * 1000 - hundreds * 100) / 10;
    int units = num % 10;

    return parseThousands(thousands) + parseHundreds(hundreds) +
           parseDozens(dozens) + parseUnits(units);
  }
};

int main() {
  Solution sol;
  assert(sol.intToRoman(3) == "III");
  assert(sol.intToRoman(4) == "IV");
  assert(sol.intToRoman(9) == "IX");
  assert(sol.intToRoman(58) == "LVIII");
  assert(sol.intToRoman(1994) == "MCMXCIV");
  assert(sol.intToRoman(246) == "CCXLVI");
  assert(sol.intToRoman(207) == "CCVII");
  assert(sol.intToRoman(1066) == "MLXVI");
  assert(sol.intToRoman(1990) == "MCMXC");
  assert(sol.intToRoman(1776) == "MDCCLXXVI");
}


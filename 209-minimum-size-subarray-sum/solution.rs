/*
https://leetcode.com/problems/minimum-size-subarray-sum

209. Minimum Size Subarray Sum

Given an array of positive integers nums and a positive integer target, return the minimal length of a subarray whose
sum is greater than or equal to target. If there is no such subarray, return 0 instead.

Example 1:

Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.

Example 2:

Input: target = 4, nums = [1,4,4]
Output: 1

Example 3:

Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0

Constraints:

1 <= target <= 10^9
1 <= nums.length <= 10^5
1 <= nums[i] <= 10^4
*/
impl Solution {
    pub fn min_sub_array_len(target: i32, nums: Vec<i32>) -> i32 {
        if target == 1 {
            return 1;
        }
        let n = nums.len();
        let mut min = i32::MAX;
        let mut sum = 0;
        let mut start = 0;
        let mut end = 0;
        while end < n {
            sum += nums[end];
            while sum >= target {
                let diff = (end - start + 1) as i32;
                min = std::cmp::min(min, diff);
                sum -= nums[start];
                start += 1;
            }
            end += 1;
        }
        if min == i32::MAX { 0 } else { min }
    }
}

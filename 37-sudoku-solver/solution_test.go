package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSolveSudoku(t *testing.T) {
	board := [][]byte{
		{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'}}
	solveSudoku(board)

	assert.Equal(t, board[0][0], byte('5'), "Test#1 failed")
	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board[i]); j++ {
			assert.NotEqual(t, board[i][j], '.', "Test#1 failed")
		}
	}
}

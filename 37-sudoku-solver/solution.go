package solution

import (
	"fmt"
)

// https://leetcode.com/problems/sudoku-solver

// Write a program to solve a Sudoku puzzle by filling the empty cells.

// A sudoku solution must satisfy all of the following rules:
// 1. Each of the digits 1-9 must occur exactly once in each row.
// 2. Each of the digits 1-9 must occur exactly once in each column.
// 3. Each of the the digits 1-9 must occur exactly once in each of the 9 3x3 sub-boxes of the grid.
// Empty cells are indicated by the character '.'.

// Note:
// - The given board contain only digits 1-9 and the character '.'.
// - You may assume that the given Sudoku puzzle will have a single unique solution.
// - The given board size is always 9x9.

const boardSize = 9

type presentSet map[byte]bool

type point2d struct {
	X int
	Y int
}

func newPoint(x int, y int) *point2d {
	p := new(point2d)
	p.X = x
	p.Y = y
	return p
}

func nextEmptyCell(board [][]byte, y int, x int) (*point2d, error) {
	for i := 0; i < boardSize; i++ {
		for j := 0; j < boardSize; j++ {
			if board[i][j] == '.' {
				return newPoint(j, i), nil
			}
		}
	}
	return nil, fmt.Errorf("not found")
}

func solveSudokuImpl(board [][]byte,
	rows [boardSize]presentSet,
	columns [boardSize]presentSet,
	blocks [boardSize / 3][boardSize / 3]presentSet,
	present int) bool {
	if present == boardSize*boardSize {
		return true
	}
	p, err := nextEmptyCell(board, 0, 0)
	if err == nil {
		for i := '1'; i <= '9'; i++ {
			c := byte(i)
			_, found := rows[p.Y][c]
			if found {
				continue
			}
			_, found = columns[p.X][c]
			if found {
				continue
			}
			_, found = blocks[p.Y/3][p.X/3][c]
			if found {
				continue
			}
			rows[p.Y][c] = true
			columns[p.X][c] = true
			blocks[p.Y/3][p.X/3][c] = true
			board[p.Y][p.X] = c
			if solveSudokuImpl(board, rows, columns, blocks, present+1) {
				return true
			}
			board[p.Y][p.X] = '.'
			delete(blocks[p.Y/3][p.X/3], c)
			delete(columns[p.X], c)
			delete(rows[p.Y], c)
		}
	}
	return false
}

func solveSudoku(board [][]byte) {
	rows := [boardSize]presentSet{}
	columns := [boardSize]presentSet{}
	blocks := [boardSize / 3][boardSize / 3]presentSet{}
	present := 0
	for i := 0; i < boardSize; i++ {
		if rows[i] == nil {
			rows[i] = make(presentSet)
		}
		for j := 0; j < boardSize; j++ {
			if columns[j] == nil {
				columns[j] = make(presentSet)
			}
			if blocks[i/3][j/3] == nil {
				blocks[i/3][j/3] = make(presentSet)
			}
			if board[i][j] != '.' {
				rows[i][board[i][j]] = true
				columns[j][board[i][j]] = true
				blocks[i/3][j/3][board[i][j]] = true
				present++
			}
		}
	}
	solveSudokuImpl(board, rows, columns, blocks, present)
}

/*
https://leetcode.com/problems/multiply-strings

Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"

Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"

Note:

* The length of both num1 and num2 is < 110.
* Both num1 and num2 contain only digits 0-9.
* Both num1 and num2 do not contain any leading zero, except the number 0 itself.
* You must not use any built-in BigInteger library or convert the inputs to integer directly.
*/
#include <cassert>
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  string multiply(string num1, string num2) {
    if (num1 == "0" || num2 == "0") return "0";
    string res;
    for (int i = num2.size() - 1, k = 0; i >= 0; --i, ++k) {
      string p(k, '0');
      int d2 = num2[i] - '0';
      int carry = 0;
      for (int j = num1.size() - 1; j >= 0; --j) {
        int d1 = num1[j] - '0';
        int x = d1 * d2 + carry;
        carry = x / 10;
        x %= 10;
        p.insert(0, 1, x + '0');
      }
      if (carry > 0) p.insert(0, 1, carry + '0');
      if (res.empty()) {
        res = p;
      } else {
        if (res.size() < p.size()) res.insert(0, p.size() - res.size(), '0');
        string s;
        int carry = 0;
        for (int i = res.size() - 1; i >= 0; --i) {
          int d2 = res[i] - '0';
          int d1 = p[i] - '0';
          int x = d1 + d2 + carry;
          carry = x / 10;
          x %= 10;
          s.insert(0, 1, x + '0');
        }
        if (carry > 0) s.insert(0, 1, carry + '0');
        res = s;
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  assert(sol.multiply("0", "1") == to_string(0 * 1));
  assert(sol.multiply("1", "1") == to_string(1 * 1));
  assert(sol.multiply("2", "3") == to_string(2 * 3));
  assert(sol.multiply("56", "65") == to_string(56 * 65));
  assert(sol.multiply("1", "65") == to_string(1 * 65));
  assert(sol.multiply("56", "0") == to_string(56 * 0));
  assert(sol.multiply("5612342134", "65423423") == "367178633453404682");
  assert(sol.multiply("123456789", "987654321") == "121932631112635269");
}


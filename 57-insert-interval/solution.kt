/*
https://leetcode.com/problems/insert-interval/

57. Insert Interval

You are given an array of non-overlapping intervals intervals where intervals[i] = [starti, endi] represent the start
and the end of the ith interval and intervals is sorted in ascending order by starti. You are also given an interval
newInterval = [start, end] that represents the start and end of another interval.

Insert newInterval into intervals such that intervals is still sorted in ascending order by starti and intervals still
does not have any overlapping intervals (merge overlapping intervals if necessary).

Return intervals after the insertion.

Example 1:

Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
Output: [[1,5],[6,9]]

Example 2:

Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].

Constraints:

0 <= intervals.length <= 10^4
intervals[i].length == 2
0 <= starti <= endi <= 10^5
intervals is sorted by starti in ascending order.
newInterval.length == 2
0 <= start <= end <= 10^5
*/
class Solution {
    fun insert(intervals: Array<IntArray>, newInterval: IntArray): Array<IntArray> {
        if (intervals.isEmpty()) return arrayOf(newInterval)
        if (intervals.size == 1) {
            val oldInterval = intervals[0]
            return when {
                isOverlapped(oldInterval, newInterval) -> arrayOf(merge(oldInterval, newInterval))
                oldInterval[0] < newInterval[0] -> arrayOf(oldInterval, newInterval)
                else -> arrayOf(newInterval, oldInterval)
            }
        }
        val result = mutableListOf<IntArray>()
        var merged = false
        var new = newInterval
        for (i in intervals) {
            when {
                before(i, new) -> {
                    if (!merged) {
                        result.add(new)
                        merged = true
                    }
                    result.add(i)
                }
                after(i, new) -> result.add(i)
                else -> new = merge(new, i)
            }
        }
        if (!merged) result.add(new)
        return result.toTypedArray()
    }

    private fun merge(i1: IntArray, i2: IntArray): IntArray {
        val start = min(i1[0], i2[0])
        val end = max(i1[1], i2[1])
        return intArrayOf(start, end)
    }

    private fun isOverlapped(i1: IntArray, i2: IntArray): Boolean {
        val r1 = IntRange(i1[0], i1[1])
        val r2 = IntRange(i2[0], i2[1])
        return r1.intersect(r2).isNotEmpty()
    }

    private fun before(t: IntArray, i: IntArray): Boolean = i[1] < t[0]
    private fun after(t: IntArray, i: IntArray): Boolean = i[0] > t[1]
}

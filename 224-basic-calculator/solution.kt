/*
https://leetcode.com/problems/basic-calculator/

224. Basic Calculator

Given a string s representing a valid expression, implement a basic calculator to evaluate it, and return the result
of the evaluation.

Note: You are not allowed to use any built-in function which evaluates strings as mathematical expressions, such as
eval().

Example 1:

Input: s = "1 + 1"
Output: 2

Example 2:

Input: s = " 2-1 + 2 "
Output: 3

Example 3:

Input: s = "(1+(4+5+2)-3)+(6+8)"
Output: 23

Constraints:

1 <= s.length <= 3 * 10^5
s consists of digits, '+', '-', '(', ')', and ' '.
s represents a valid expression.
'+' is not used as a unary operation (i.e., "+1" and "+(2 + 3)" is invalid).
'-' could be used as a unary operation (i.e., "-1" and "-(2 + 3)" is valid).
There will be no two consecutive operators in the input.
Every number and running calculation will fit in a signed 32-bit integer.
*/
class Solution {
    fun calculate(s: String): Int {
        var res = 0
        var operation = 1
        var parenSign = 1
        val stack = LinkedList<Int>()
        val l = s.length
        var i = 0
        loop@ while (i < l) {
            when (s[i++]) {
                ' ' -> {}
                '(' -> {
                    stack.push(operation)
                    if (operation < 0) parenSign *= -1
                    operation = 1
                }
                ')' -> if (stack.pop() < 0) parenSign *= -1
                '+' -> operation = 1
                '-' -> operation = -1
                else -> {
                    var n = s[i - 1] - '0'
                    while (i < l && '0' <= s[i] && s[i] <= '9')
                        n = n * 10 + (s[i++] - '0')
                    res += n * parenSign * operation
                    continue@loop
                }
            }
        }
        return res
    }
}

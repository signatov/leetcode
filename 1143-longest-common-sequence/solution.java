/*
https://leetcode.com/problems/longest-common-subsequence/

1143. Longest Common Subsequence

Given two strings text1 and text2, return the length of their longest common subsequence. If there is no common subsequence, return 0.

A subsequence of a string is a new string generated from the original string with some characters (can be none) deleted without changing the relative order of the remaining characters.

For example, "ace" is a subsequence of "abcde".
A common subsequence of two strings is a subsequence that is common to both strings.

Example 1:

Input: text1 = "abcde", text2 = "ace"
Output: 3
Explanation: The longest common subsequence is "ace" and its length is 3.

Example 2:

Input: text1 = "abc", text2 = "abc"
Output: 3
Explanation: The longest common subsequence is "abc" and its length is 3.

Example 3:

Input: text1 = "abc", text2 = "def"
Output: 0
Explanation: There is no such common subsequence, so the result is 0.

Constraints:

1 <= text1.length, text2.length <= 1000
text1 and text2 consist of only lowercase English characters.
*/
class Solution {
    private static class Context {
        final String text1;
        final String text2;
        final int cache[][];

        Context(String text1, String text2) {
            this.text1 = text1;
            this.text2 = text2;
            this.cache = new int[text1.length()][text2.length()];
            for (var line : cache) Arrays.fill(line, -1);
        }

        int get(int i, int j) {
            return cache[i][j];
        }

        int set(int i, int j, int value) {
            return cache[i][j] = value;
        }
    }

    public int longestCommonSubsequence(String text1, String text2) {
        if (text1.length() == 0 || text2.length() == 0) return 0;
        if (text1.equals(text2)) return text1.length();
        var context = new Context(text1, text2);
        return longestCommonSubsequence(context, 0, 0);
    }

    private int longestCommonSubsequence(Context c, int i, int j) {
        if (i < 0 || j < 0 || i >= c.text1.length() || j >= c.text2.length()) return 0;
        var r = c.get(i, j);
        if (r != -1) return r;

        int v = longestCommonSubsequence(c, i + 1, j + 1);
        if (c.text1.charAt(i) == c.text2.charAt(j))
            return c.set(i, j, 1 + v);
        int v1 = longestCommonSubsequence(c, i + 1, j);
        int v2 = longestCommonSubsequence(c, i, j + 1);
        return c.set(i, j, Math.max(v, Math.max(v1, v2)));
    }
}


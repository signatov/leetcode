/*
https://leetcode.com/problems/course-schedule/

207. Course Schedule

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array
prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course
ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take.
To take course 1 you should have finished course 0. So it is possible.

Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take.
To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it
is impossible.

Constraints:

1 <= numCourses <= 2000
0 <= prerequisites.length <= 5000
prerequisites[i].length == 2
0 <= ai, bi < numCourses
All the pairs prerequisites[i] are unique.
*/
class Solution {
    // [1, 0] - To take course 1 you should have finished course 0.
    fun canFinish(numCourses: Int, prerequisites: Array<IntArray>): Boolean {
        if (numCourses == 1 || prerequisites.isEmpty()) return true
        val courses = Array(numCourses) { ArrayList<Int>() }
        val dependencies = IntArray(numCourses)
        for (course in prerequisites) {
            if (course.hasSelfCycle()) return false
            courses[course[1]].add(course[0])
            ++dependencies[course[0]]
        }
        val queue = prepareCoursesQueue(dependencies)
        var taken = 0
        while (queue.isNotEmpty()) {
            val course = queue.removeFirst()
            ++taken
            for (c in courses[course])
                if (--dependencies[c] == 0)
                    queue.addLast(c)
        }
        return taken == numCourses
    }

    private fun prepareCoursesQueue(dependencies: IntArray): ArrayDeque<Int> {
        val queue = ArrayDeque<Int>()
        for (i in dependencies.indices)
            if (dependencies[i] == 0)
                queue.addLast(i)
        return queue
    }

    private fun IntArray.hasSelfCycle(): Boolean = this[0] == this[1]
}

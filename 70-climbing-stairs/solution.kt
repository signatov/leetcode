/*
https://leetcode.com/problems/climbing-stairs/

70. Climbing Stairs

You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

Example 1:

Input: n = 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps

Example 2:

Input: n = 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step

Constraints:

1 <= n <= 45
*/
class Solution {
    fun climbStairs(n: Int): Int = climbStairsImpl(n)

    private fun climbStairsImpl(n: Int): Int = when {
        n < 0 -> 0
        n == 0 -> 1
        else -> climbStairsCached(n - 1) + climbStairsCached(n - 2)
    }

    private val cache = Array(46) { -1 }

    private fun climbStairsCached(n: Int): Int = when {
        n < 0 -> 0
        n == 0 -> 1
        else -> {
            if (cache[n] == -1)
                cache[n] = climbStairsImpl(n)
            cache[n]
        }
    }
}

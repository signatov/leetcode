/*
https://leetcode.com/problems/evaluate-division

399. Evaluate Division

You are given an array of variable pairs equations and an array of real numbers values, where equations[i] = [Ai, Bi]
and values[i] represent the equation Ai / Bi = values[i]. Each Ai or Bi is a string that represents a single variable.

You are also given some queries, where queries[j] = [Cj, Dj] represents the jth query where you must find the answer
for Cj / Dj = ?.

Return the answers to all queries. If a single answer cannot be determined, return -1.0.

Note: The input is always valid. You may assume that evaluating the queries will not result in division by zero and
that there is no contradiction.

Note: The variables that do not occur in the list of equations are undefined, so the answer cannot be determined for
them.

Example 1:

Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],
["x","x"]]
Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]
Explanation:
Given: a / b = 2.0, b / c = 3.0
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ?
return: [6.0, 0.5, -1.0, 1.0, -1.0 ]
note: x is undefined => -1.0

Example 2:

Input: equations = [["a","b"],["b","c"],["bc","cd"]], values = [1.5,2.5,5.0], queries = [["a","c"],["c","b"],
["bc","cd"],["cd","bc"]]
Output: [3.75000,0.40000,5.00000,0.20000]

Example 3:

Input: equations = [["a","b"]], values = [0.5], queries = [["a","b"],["b","a"],["a","c"],["x","y"]]
Output: [0.50000,2.00000,-1.00000,-1.00000]

Constraints:

1 <= equations.length <= 20
equations[i].length == 2
1 <= Ai.length, Bi.length <= 5
values.length == equations.length
0.0 < values[i] <= 20.0
1 <= queries.length <= 20
queries[i].length == 2
1 <= Cj.length, Dj.length <= 5
Ai, Bi, Cj, Dj consist of lower case English letters and digits.
*/
use std::collections::{BTreeMap, HashSet, VecDeque};

struct Graph {
    index_map: BTreeMap<String, usize>,
    weights: Vec<Vec<Option<f64>>>,
}

struct Node(usize, f64);

impl Graph {
    pub fn with_variables(variables: &HashSet<String>) -> Graph {
        let index_map: BTreeMap<String, usize> = variables
            .into_iter()
            .enumerate()
            .map(|x| (x.1.to_string(), x.0))
            .collect();
        let weights = vec![vec![Option::None; variables.len()]; variables.len()];
        Graph { index_map, weights }
    }

    pub fn update(&mut self, equation: &Vec<String>, v: f64) {
        let xi = *self.index_map.get(&equation[0]).unwrap();
        let yi = *self.index_map.get(&equation[1]).unwrap();
        self.weights[xi][yi] = Some(v);
        if v != 0.0 {
            self.weights[yi][xi] = Some(1.0 / v);
        }
    }

    pub fn search(&self, query: &Vec<String>) -> f64 {
        if !self.index_map.contains_key(&query[0]) || !self.index_map.contains_key(&query[1]) {
            return -1.0;
        }
        if query[0] == query[1] {
            return 1.0;
        }
        let start = *self.index_map.get(&query[0]).unwrap();
        let end = *self.index_map.get(&query[1]).unwrap();
        let mut visited: HashSet<usize> = HashSet::new();
        visited.insert(start);
        let mut queue: VecDeque<Node> = VecDeque::new();
        queue.push_back(Node(start, 1.0));
        let n = self.index_map.len();
        while let Some(node) = queue.pop_front() {
            let i = node.0;
            let w = node.1;
            for j in 0..n {
                if j != i && !visited.contains(&j) {
                    if let Some(x) = self.weights[i][j] {
                        let r = w * x;
                        if j == end {
                            return r;
                        }
                        queue.push_back(Node(j, r));
                        visited.insert(j);
                    }
                }
            }
        }
        -1.0
    }
}

impl Solution {
    pub fn calc_equation(
        equations: Vec<Vec<String>>,
        values: Vec<f64>,
        queries: Vec<Vec<String>>,
    ) -> Vec<f64> {
        let g = Solution::build_graph(&equations, &values);
        queries.into_iter().map(|q| g.search(&q)).collect()
    }

    fn build_graph(equations: &Vec<Vec<String>>, values: &Vec<f64>) -> Graph {
        let names: Vec<String> = equations.into_iter().flatten().cloned().collect();
        let variables: HashSet<String> = names.into_iter().collect();
        let mut graph = Graph::with_variables(&variables);
        for i in equations.into_iter().enumerate() {
            let v = values[i.0];
            let e = &i.1;
            graph.update(e, v);
        }
        graph
    }
}

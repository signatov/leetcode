/*
Given a positive integer, output its complement number. The complement strategy is to flip the bits of its binary representation.
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static bool has_bit(int n, int i) {
    return ((unsigned)n & (1 << i)) != 0;
  }
  static int flip_bit(int n, int i) {
    return has_bit(n, i) ? n & ~(1 << i) : n | (1 << i);
  }
  static int max_set_bit(int n) {
    for (int i = 31; i >= 0; --i) {
      if (n & (1 << i)) return i;
    }
    return 0;
  }
public:
  int findComplement(int n) {
    for (int i = 0; i <= max_set_bit(n); ++i) {
      n = flip_bit(n, i);
    }
    return n;
  }
};

int main() {
  Solution sol;
  cout << sol.findComplement(5) << endl;
  cout << sol.findComplement(1) << endl;
}

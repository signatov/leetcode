/*
Given a binary search tree and the lowest and highest boundaries as L and R, trim the tree so that all its elements lies in [L, R] (R >= L).
You might need to change the root of the tree, so the result should return the new root of the trimmed binary search tree.
*/
#include <iostream>
#include <cassert>
#include "../treenode.h"

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
  static void deleteNode(TreeNode *node) {
    // delete node;
  }
  static bool isNodeInRange(TreeNode *node, int L, int R) {
    return L <= node->val && node->val <= R;
  }
public:
  TreeNode* trimBST(TreeNode *root, int L, int R) {
    if (!root)
      return root;

    root->left = trimBST(root->left, L, R);
    root->right = trimBST(root->right, L, R);
    return isNodeInRange(root, L, R) ? root : root->val < L ? root->right : root->left;
  }
};

void check1() {
  TreeNode * root = new TreeNode(1);
  root->left = new TreeNode(0);
  root->right = new TreeNode(2);
  cout << "==============================================================\n";
  displayBST(root);
  Solution sol;
  TreeNode * trimmed = sol.trimBST(root, 1, 2);
  cout << "==============================================================\n";
  displayBST(trimmed);
  assert(root->val == 1);
  assert(root->left == NULL);
  assert(root->right && root->right->val == 2);
  assert(!root->right->left && !root->right->right);
  deleteBST(root);
};

void check2() {
  TreeNode * root = new TreeNode(3);
  root->left = new TreeNode(0);
  root->right = new TreeNode(4);
  root->left->right = new TreeNode(2);
  root->left->right->left = new TreeNode(1);
  cout << "==============================================================\n";
  displayBST(root);
  Solution sol;
  TreeNode * trimmed = sol.trimBST(root, 1, 3);
  cout << "==============================================================\n";
  displayBST(trimmed);
  assert(root->val == 3);
  assert(root->right == NULL);
  assert(root->left && root->left->val == 2);
  assert(root->left->left && root->left->left->val == 1);
  assert(root->left->right == NULL);
  deleteBST(root);
};

int main() {
  check1();
  check2();
}


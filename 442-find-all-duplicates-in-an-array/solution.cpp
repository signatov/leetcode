/*
Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

Find all the elements that appear twice in this array.

Could you do it without extra space and in O(n) runtime?
*/
#include <iostream>
#include <vector>
#include <cstdlib>
#include "../int_vector.h"

using namespace std;

class Solution {
  static inline void mark(vector<int> &nums, int index) {
    nums[index] = -nums[index];
  }
  static inline bool isDuplicate(vector<int> &nums, int index) {
    return nums[index] > 0;
  }
public:
  vector<int> findDuplicates(vector<int> &nums) {
    vector<int> res;
    if (nums.empty()) return res;
    for (int i = 0; i < nums.size(); ++i) {
      int index = abs(nums[i]) - 1;
      mark(nums, index);
      if (isDuplicate(nums, index)) {
        res.push_back(abs(nums[i]));
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  vector<int> v1{};
  vector<int> v2{4,3,2,7,8,2,3,1};
  vector<int> v3{10,2,5,10,9,1,1,4,3,7};
  printIntVector(sol.findDuplicates(v1), true);
  printIntVector(sol.findDuplicates(v2), true);
  printIntVector(sol.findDuplicates(v3), true);
}


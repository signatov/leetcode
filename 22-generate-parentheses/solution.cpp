/*
https://leetcode.com/problems/generate-parentheses

Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]
*/
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
  static void generate(vector<string> &v, int n, string s = "", int opened = 0, int closed = 0) {
    if (closed == n) {
      v.push_back(s);
      return;
    }
    if (opened > closed) {
      generate(v, n, s + ')', opened, closed + 1);
    }
    if (opened < n) {
      generate(v, n, s + '(', opened + 1, closed);
    }
  }
public:
  vector<string> generateParenthesis(int n) {
    vector<string> res;
    if (n <= 0) return res;
    generate(res, n);
    return res;
  }
};

void test(Solution &sol, int n) {
  const vector<string> &v = sol.generateParenthesis(n);
  cout << "N: " << v.size() << endl;
  for (auto s : v) {
    cout << s << endl;
  }
  cout << endl;
}

int main() {
  Solution sol;
  test(sol, 0);
  test(sol, 1);
  test(sol, 2);
  test(sol, 3);
  test(sol, 4);
}

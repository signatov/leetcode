/*
https://leetcode.com/problems/word-search-ii/

212. Word Search II

Given an m x n board of characters and a list of strings words, return all words on the board.

Each word must be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or
vertically neighboring. The same letter cell may not be used more than once in a word.

Example 1:

Input: board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]], words = ["oath","pea","eat",
"rain"]
Output: ["eat","oath"]

Example 2:

Input: board = [["a","b"],["c","d"]], words = ["abcb"]
Output: []

Constraints:

m == board.length
n == board[i].length
1 <= m, n <= 12
board[i][j] is a lowercase English letter.
1 <= words.length <= 3 * 10^4
1 <= words[i].length <= 10
words[i] consists of lowercase English letters.
All the strings of words are unique.
*/
class Solution {
    companion object {
        private const val VISITED = '\u0000'
        private val DELTAS = arrayOf(
            Pos(-1, 0), Pos(1, 0), Pos(0, -1), Pos(0, 1))
    }

    fun findWords(board: Array<CharArray>, words: Array<String>): List<String> {
        val b = Board(board)

        fun buildTrie(): Node {
            val root = Node()
            for (word in words) {
                var p = root
                for (c in word)
                    p = p.nextOrCreate(c)
                p.word = word  // All the strings of words are unique
            }
            return root
        }

        val res = mutableSetOf<String>()
        val root = buildTrie()

        fun visit(p: Pos, action: () -> Unit) {
            val c = b[p]
            b[p] = VISITED
            action.invoke()
            b[p] = c
        }

        fun search(i: Pos, p: Node) {
            val c = b[i]
            if (c.isVisited() || p.next(c) == null) return
            val q = p.next(c)!!
            q.word?.let(res::add)
            visit(i) {
                val n = b.neighbors(i)
                for (j in n) search(j, q)
            }
        }

        for (y in 0..b.height) {
            for (x in 0..b.width) {
                search(Pos(y, x), root)
            }
        }

        return res.toList()
    }

    private fun Char.isVisited(): Boolean = this == VISITED

    class Node {
        private val children = arrayOfNulls<Node>('z' - 'a' + 1)
        var word: String? = null

        fun next(c: Char): Node? = children[c - 'a']

        fun nextOrCreate(c: Char): Node {
            if (children[c - 'a'] == null) children[c - 'a'] = Node()
            return next(c)!!
        }
    }

    class Board(private val board: Array<CharArray>) {
        val height = board.size - 1
        val width = board[0].size - 1

        operator fun get(p: Pos): Char = board[p.y][p.x]

        operator fun set(p: Pos, v: Char) {
            board[p.y][p.x] = v
        }

        fun neighbors(p: Pos) = sequence {
            for (i in DELTAS) {
                val n = p + i
                if (n.y in 0..height && n.x in 0..width)
                    yield(n)
            }
        }
    }

    data class Pos(val y: Int, val x: Int) {
        operator fun plus(d: Pos) = Pos(y + d.y, x + d.x)
    }
}

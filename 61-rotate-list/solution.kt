/*
https://leetcode.com/problems/rotate-list/

61. Rotate List

Given the head of a linked list, rotate the list to the right by k places. 

Example 1:

Input: head = [1,2,3,4,5], k = 2
Output: [4,5,1,2,3]

Example 2:

Input: head = [0,1,2], k = 4
Output: [2,0,1]

Constraints:

The number of nodes in the list is in the range [0, 500].
-100 <= Node.val <= 100
0 <= k <= 2 * 10^9
*/
/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    fun rotateRight(head: ListNode?, k: Int): ListNode? {
        if (head == null) return null
        val size = len(head)
        val index = k % size
        var p = head
        var q = head
        repeat (index) { p = p?.next }
        while (p?.next != null) {
            p = p?.next
            q = q?.next
        }
        p?.next = head
        val res = q?.next
        q?.next = null
        return res
    }

    private fun len(head: ListNode): Int {
        var count = 0
        var p: ListNode? = head
        while (p != null) {
            ++count
            p = p.next
        }
        return count
    }
}

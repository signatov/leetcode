/*
https://leetcode.com/problems/pascals-triangle

Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it.

Example:
Input: 5
Output:
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
*/
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

void printTriangle(const vector<vector<int>> & tri) {
  size_t len = tri.size();
  for (size_t i = 0; i < len; ++i) {
    cout << string((len - i - 1) * 3 / 2, ' ');
    for (size_t j = 0; j < tri[i].size(); ++j)
      cout << setw(3) << tri[i][j];
    cout << "\n";
  }
  cout << endl;
}

class Solution {
public:
  vector<vector<int>> generate(int numRows) {
    if (numRows == 0) return vector<vector<int>>();
    vector<vector<int>> res(numRows, vector<int>());
    res[0].push_back(1);
    for (int i = 1; i < numRows; ++i) {
      res[i].push_back(1);
      for (int j = 0; j < i - 1; ++j) {
        res[i].push_back(res[i - 1][j] + res[i - 1][j + 1]);
      }
      res[i].push_back(1);
    }
    return res;
  }
};

int main() {
  Solution sol;
  const vector<vector<int>> & tri = sol.generate(5);
  printTriangle(tri);
}


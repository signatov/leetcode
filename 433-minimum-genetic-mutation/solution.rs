/*
https://leetcode.com/problems/minimum-genetic-mutation

433. Minimum Genetic Mutation

A gene string can be represented by an 8-character long string, with choices from 'A', 'C', 'G', and 'T'.

Suppose we need to investigate a mutation from a gene string startGene to a gene string endGene where one mutation is
defined as one single character changed in the gene string.

For example, "AACCGGTT" --> "AACCGGTA" is one mutation.
There is also a gene bank bank that records all the valid gene mutations. A gene must be in bank to make it a valid
gene string.

Given the two gene strings startGene and endGene and the gene bank bank, return the minimum number of mutations needed
to mutate from startGene to endGene. If there is no such a mutation, return -1.

Note that the starting point is assumed to be valid, so it might not be included in the bank.

Example 1:

Input: startGene = "AACCGGTT", endGene = "AACCGGTA", bank = ["AACCGGTA"]
Output: 1

Example 2:

Input: startGene = "AACCGGTT", endGene = "AAACGGTA", bank = ["AACCGGTA","AACCGCTA","AAACGGTA"]
Output: 2

Constraints:

0 <= bank.length <= 10
startGene.length == endGene.length == bank[i].length == 8
startGene, endGene, and bank[i] consist of only the characters ['A', 'C', 'G', 'T'].
*/
use std::collections::{BTreeMap, VecDeque};

struct Graph {
    index_map: BTreeMap<String, usize>,
    weights: Vec<Vec<bool>>,
}

impl Graph {
    fn with_genes(bank: &Vec<String>) -> Graph {
        let n = bank.len();
        let index_map: BTreeMap<String, usize> = bank
            .into_iter()
            .enumerate()
            .map(|x| (x.1.to_string(), x.0))
            .collect();
        let mut weights = vec![vec![false; n]; n];
        for i in 0..bank.len() - 1 {
            for j in i..bank.len() {
                let g1 = &bank[i];
                let g2 = &bank[j];
                if Graph::is_connected_genes(g1, g2) {
                    weights[i][j] = true;
                    weights[j][i] = true;
                }
            }
        }
        Graph { index_map, weights }
    }

    fn is_connected(&self, i: usize, j: usize) -> bool {
        debug_assert!(i < self.weights.len());
        debug_assert!(j < self.weights.len());
        self.weights[i][j]
    }

    fn is_connected_genes(g1: &String, g2: &String) -> bool {
        let mut diff = 0;
        let b1 = g1.as_bytes();
        let b2 = g2.as_bytes();
        for i in 0..8 {
            diff += if b1[i] != b2[i] { 1 } else { 0 };
            if diff > 1 {
                return false;
            }
        }
        true
    }
}

struct Travelsal {
    n: usize,
    visited: Vec<bool>,
    queue: VecDeque<usize>,
}

impl Travelsal {
    fn new(n: usize) -> Travelsal {
        Travelsal {
            n,
            visited: vec![false; n],
            queue: VecDeque::new(),
        }
    }

    fn visit(&mut self, i: usize) {
        debug_assert!(i < self.n);
        self.visited[i] = true;
        self.queue.push_back(i);
    }

    fn is_empty(&self) -> bool {
        self.queue.is_empty()
    }

    fn is_visited(&self, i: usize) -> bool {
        debug_assert!(i < self.n);
        self.visited[i]
    }

    fn points_to_check(&self) -> usize {
        self.queue.len()
    }

    fn next_point(&mut self) -> usize {
        debug_assert!(!self.is_empty());
        self.queue.pop_front().unwrap()
    }
}

impl Solution {
    pub fn min_mutation(start_gene: String, end_gene: String, bank: Vec<String>) -> i32 {
        if start_gene == end_gene {
            return 0;
        }
        if !bank.contains(&end_gene) {
            return -1;
        }
        let g = Graph::with_genes(&bank);
        let last = g.index_map.get(&end_gene).unwrap();
        let n = bank.len();
        let mut path = Travelsal::new(n);
        for i in 0..n {
            if Graph::is_connected_genes(&start_gene, &bank[i]) {
                path.visit(i);
            }
        }
        let mut res = 1;
        while !path.is_empty() {
            let mut size = path.points_to_check();
            while size > 0 {
                let i = path.next_point();
                if last == &i {
                    return res;
                }
                for j in 0..n {
                    if g.is_connected(i, j) && !path.is_visited(j) {
                        path.visit(j);
                    }
                }
                size -= 1;
            }
            res += 1;
        }
        -1
    }
}

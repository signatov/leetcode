/*
https://leetcode.com/problems/reorder-list/

143. Reorder List

You are given the head of a singly linked-list. The list can be represented as:

L0 → L1 → … → Ln - 1 → Ln
Reorder the list to be on the following form:

L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
You may not modify the values in the list's nodes. Only nodes themselves may be changed.

Example 1:

Input: head = [1,2,3,4]
Output: [1,4,2,3]

Example 2:

Input: head = [1,2,3,4,5]
Output: [1,5,2,4,3]

Constraints:

The number of nodes in the list is in the range [1, 5 * 10^4].
1 <= Node.val <= 1000
*/
/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    fun reorderList(head: ListNode?) {
        if (head?.next == null) return

        val indexes = buildIndexes(head)
        var i = 0
        var j = indexes.size - 1
        while (i < j) {
            indexes[i++].next = indexes[j]
            indexes[j--].next = indexes[i]
        }
        indexes[i].next = null
    }

    private fun buildIndexes(head: ListNode?): Array<ListNode> {
        val indexes = mutableListOf<ListNode>()
        var p = head
        while (p != null) {
            indexes.add(p)
            p = p.next
        }
        return indexes.toTypedArray()
    }
}

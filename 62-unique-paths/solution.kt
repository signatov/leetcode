/*
https://leetcode.com/problems/unique-paths/

62. Unique Paths

There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot
tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at
any point in time.

Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the
bottom-right corner.

The test cases are generated so that the answer will be less than or equal to 2 * 10^9.

Example 1:

Input: m = 3, n = 7
Output: 28

Example 2:

Input: m = 3, n = 2
Output: 3
Explanation: From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Down -> Down
2. Down -> Down -> Right
3. Down -> Right -> Down

Constraints:

1 <= m, n <= 100
*/
class Solution {
    private val UNINIT = -1
    fun uniquePaths(m: Int, n: Int): Int {
        val dp = Array(m + 1) { IntArray(n + 1) { UNINIT } }
        fun findPaths(x: Int = 1, y: Int = 1): Int = when {
            y > m || x > n -> 0
            y == m && x == n -> 1
            dp[y][x] != UNINIT -> dp[y][x]
            else -> {
                val r1 = findPaths(x + 1, y)
                val r2 = findPaths(x, y + 1)
                dp[y][x] = r1 + r2
                dp[y][x]
            }
        }
        return findPaths()
    }
}

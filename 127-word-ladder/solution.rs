/*
https://leetcode.com/problems/word-ladder

127. Word Ladder

A transformation sequence from word beginWord to word endWord using a dictionary wordList is a sequence of words
beginWord -> s1 -> s2 -> ... -> sk such that:

Every adjacent pair of words differs by a single letter.
Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in wordList.
sk == endWord
Given two words, beginWord and endWord, and a dictionary wordList, return the number of words in the shortest
transformation sequence from beginWord to endWord, or 0 if no such sequence exists.

Example 1:

Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
Output: 5
Explanation: One shortest transformation sequence is "hit" -> "hot" -> "dot" -> "dog" -> cog", which is 5 words long.

Example 2:

Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
Output: 0
Explanation: The endWord "cog" is not in wordList, therefore there is no valid transformation sequence.

Constraints:

1 <= beginWord.length <= 10
endWord.length == beginWord.length
1 <= wordList.length <= 5000
wordList[i].length == beginWord.length
beginWord, endWord, and wordList[i] consist of lowercase English letters.
beginWord != endWord
All the words in wordList are unique.
*/
use std::collections::{HashSet, VecDeque};

#[derive(Default)]
struct Travelsal {
    visited: HashSet<String>,
    queue: VecDeque<String>,
}

impl Travelsal {
    fn visit(&mut self, word: &String) {
        self.visited.insert(word.clone());
        self.queue.push_back(word.clone());
    }

    fn is_empty(&self) -> bool {
        self.queue.is_empty()
    }

    fn len(&self) -> usize {
        self.queue.len()
    }

    fn is_visited(&self, word: &String) -> bool {
        self.visited.contains(word)
    }

    fn next(&mut self) -> String {
        debug_assert!(!self.is_empty());
        self.queue.pop_front().unwrap()
    }
}

impl Solution {
    pub fn ladder_length(begin_word: String, end_word: String, word_list: Vec<String>) -> i32 {
        let n = word_list.len();
        let word_set: HashSet<String> = HashSet::from_iter(word_list.clone());
        let l = begin_word.len();
        let mut path: Travelsal = Default::default();
        for i in 0..n {
            if Solution::is_connected_words(&begin_word, &word_list[i]) {
                path.visit(&word_list[i]);
            }
        }
        let mut res = 2;
        while !path.is_empty() {
            for _ in 0..path.len() {
                let word = path.next();
                if end_word == word {
                    return res;
                }
                for i in 0..l {
                    for nc in (b'a'..=b'z').filter(|&c| c != word.as_bytes()[i]) {
                        let mut word = word.as_bytes().to_vec();
                        word[i] = nc;
                        let word = unsafe { String::from_utf8_unchecked(word) };
                        if word_set.contains(&word) && !path.is_visited(&word) {
                            path.visit(&word);
                        }
                    }
                }
            }
            res += 1;
        }
        0
    }

    fn is_connected_words(w1: &String, w2: &String) -> bool {
        let mut diff = 0;
        let b1 = w1.as_bytes();
        let b2 = w2.as_bytes();
        for i in 0..b1.len() {
            diff += if b1[i] != b2[i] { 1 } else { 0 };
            if diff > 1 {
                return false;
            }
        }
        true
    }
}

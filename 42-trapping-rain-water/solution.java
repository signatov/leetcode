/*
https://leetcode.com/problems/trapping-rain-water/

42. Trapping Rain Water

Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it can trap after raining.

 Example 1:

Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
Output: 6
Explanation: The above elevation map (black section) is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped.

Example 2:

Input: height = [4,2,0,3,2,5]
Output: 9
 

Constraints:

n == height.length
1 <= n <= 2 * 10^4
0 <= height[i] <= 10^5
*/
class Solution {
    public int trap(int[] height) {
        var l = fillLeft(height);
        var r = fillRight(height);
        int trapped = 0;
        for (int i = 0; i < height.length; ++i) {
            int h = height[i];
            int level = Math.min(l[i], r[i]);
            trapped += level - h;
        }
        return trapped;
    }

    private static int[] fillLeft(int[] height) {
        return fill(height, 0, 1);
    }

    private static int[] fillRight(int[] height) {
        return fill(height, height.length - 1, -1);
    }

    private static int[] fill(int[] height, int pos0, int delta) {
        var l = height.length;
        int[] max = new int[l];
        max[pos0] = height[pos0];
        int end = delta > 0 ? l : -1;
        for (int i = pos0 + delta; i != end; i+= delta)
            max[i] = Math.max(height[i], max[i - delta]);
        return max;
    }
}


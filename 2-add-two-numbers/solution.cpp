/*
https://leetcode.com/problems/add-two-numbers

You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit.
Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.
*/
#include <cassert>
#include <iostream>
#include "../listnode.h"

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
  static void addNode(ListNode *&head, ListNode *&tail, ListNode *n) {
    if (!head) {
      head = tail = n;
    } else {
      tail->next = n;
      tail = n;
    }
  }
public:
  ListNode *addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode *res = NULL, *tail = NULL;
    ListNode *i = l1, *j = l2;
    int carry = 0;
    for (;;) {
      if (!i && !j) {
        if (carry != 0) {
          addNode(res, tail, new ListNode(carry));
        }
        break;
      }
      int v1 = i ? i->val : 0;
      int v2 = j ? j->val : 0;
      int v = v1 + v2 + carry;
      carry = v / 10;
      v %= 10;
      addNode(res, tail, new ListNode(v));
      if (i) { i = i->next; }
      if (j) { j = j->next; }
    }
    return res;
  }
};

void test1() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{2,4,3}), &deleteList};
  ListPtr l2{makeList(initializer_list<int>{5,6,4}), &deleteList};
  ListPtr res{sol.addTwoNumbers(l1.get(), l2.get()), &deleteList};
  printList(l1.get());
  printList(l2.get());
  printList(res.get());
  assert(res);
  assert(res.get()->val == 7);
  assert(res.get()->next != NULL);
  assert(res.get()->next->val == 0);
  assert(res.get()->next->next != NULL);
  assert(res.get()->next->next->val == 8);
}

int main() {
  test1();
}


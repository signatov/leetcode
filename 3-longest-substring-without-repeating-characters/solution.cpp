/*
https://leetcode.com/problems/longest-substring-without-repeating-characters

Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring,
"pwke" is a subsequence and not a substring.
*/
#include <algorithm>
#include <array>
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  int lengthOfLongestSubstring(string s) {
    int res = 0;
    if (s.empty()) return res;
    array<int, 256> set;
    set.fill(0);
    for (int i = 0, j = 0; i < s.size(); ++i) {
      int ch = s[i];
      j = max(j, set[ch]);
      res = max(res, i - j + 1);
      set[ch] = i + 1;
    }
    return res;
  }
};

int main() {
  Solution sol;
  cout << sol.lengthOfLongestSubstring("abcabcbb") << endl;
  cout << sol.lengthOfLongestSubstring("bbbbb") << endl;
  cout << sol.lengthOfLongestSubstring("pwwkew") << endl;
}

/*
TinyURL is a URL shortening service where you enter a URL such as https://leetcode.com/problems/design-tinyurl and it returns a short URL such as http://tinyurl.com/4e9iAk.

Design the encode and decode methods for the TinyURL service. There is no restriction on how your encode/decode algorithm should work.
You just need to ensure that a URL can be encoded to a tiny URL and the tiny URL can be decoded to the original URL.
*/
#include <iostream>
#include <string>
#include <cstddef>
#include <map>
#include <cassert>

using namespace std;

class Solution {
  const string base{"http://tinyurl.com/"};
  map<uint64_t, string> h;
  uint64_t index{0};
public:
  // Encodes a URL to a shortened URL.
  string encode(string longUrl) {
    string url{base};
    uint64_t i = index++;
    h[i] = longUrl;
    url += to_string(i);
    return url;
  }

  // Decodes a shortened URL to its original URL.
  string decode(string shortUrl) {
    string suffix = shortUrl.erase(0, base.size());
    uint64_t i = stoull(suffix);
    return h[i];
  }
};

int main() {
  Solution sol;
  assert(sol.decode(sol.encode("ya.ru")) == "ya.ru");
  assert(sol.decode(sol.encode("http://ya.ru")) == "http://ya.ru");
  assert(sol.decode(sol.encode("https://ya.ru")) == "https://ya.ru");
  assert(sol.decode(sol.encode("ya.ru/?q=Abc+def+xyz")) == "ya.ru/?q=Abc+def+xyz");
  assert(sol.decode(sol.encode("fb.com")) == "fb.com");
}

#ifndef __INT_VECTOR_H__
#define __INT_VECTOR_H__

#include <iostream>
#include <vector>

void printIntVector(const std::vector<int> &v, bool newLine = false) {
  for (int x : v) { std::cout << x << " "; }
  if (newLine) { std::cout << std::endl; }
}

#endif // __INT_VECTOR_H__


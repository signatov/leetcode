/*
https://leetcode.com/problems/isomorphic-strings/

205. Isomorphic Strings

Given two strings s and t, determine if they are isomorphic.

Two strings s and t are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No
two characters may map to the same character, but a character may map to itself.

Example 1:

Input: s = "egg", t = "add"
Output: true

Example 2:

Input: s = "foo", t = "bar"
Output: false

Example 3:

Input: s = "paper", t = "title"
Output: true

Constraints:

1 <= s.length <= 5 * 10^4
t.length == s.length
s and t consist of any valid ascii character.
*/
class Solution {
    fun isIsomorphic(s: String, t: String): Boolean {
        val s1 = IntArray(256)
        val s2 = IntArray(256)
        for (i in s.indices) {
            val c1 = s[i].toInt()
            val c2 = t[i].toInt()
            if (s1[c1] != s2[c2]) return false
            s1[c1] = i + 1  // 0 -- default value (unused)
            s2[c2] = i + 1
        }
        return true
    }
}

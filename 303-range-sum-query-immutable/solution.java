/*
https://leetcode.com/problems/range-sum-query-immutable/

303. Range Sum Query - Immutable

Given an integer array nums, handle multiple queries of the following type:

Calculate the sum of the elements of nums between indices left and right inclusive where left <= right.

Implement the NumArray class:

- NumArray(int[] nums) Initializes the object with the integer array nums.
- int sumRange(int left, int right) Returns the sum of the elements of nums between indices left and right inclusive (i.e. nums[left] + nums[left + 1] + ... + nums[right]).

Example 1:

Input
["NumArray", "sumRange", "sumRange", "sumRange"]
[[[-2, 0, 3, -5, 2, -1]], [0, 2], [2, 5], [0, 5]]
Output
[null, 1, -1, -3]

Explanation
NumArray numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
numArray.sumRange(0, 2); // return (-2) + 0 + 3 = 1
numArray.sumRange(2, 5); // return 3 + (-5) + 2 + (-1) = -1
numArray.sumRange(0, 5); // return (-2) + 0 + 3 + (-5) + 2 + (-1) = -3

Constraints:

1 <= nums.length <= 10^4
-10^5 <= nums[i] <= 10^5
0 <= left <= right < nums.length
At most 10^4 calls will be made to sumRange.
*/
class NumArray {
    private static class SegmentTree {
        final int[] segments;
        final int n;

        private SegmentTree(int n) {
            this(new int[n << 2], n);
        }

        private SegmentTree(int[] segments, int n) {
            this.segments = segments;
            this.n = n;
        }

        private int set(int index, int value) {
            return segments[index] = value;
        }

        private int get(int index) {
            return segments[index];
        }

        int query(int left, int right) {
            if (left > right) return 0;
            var segment = new Segment(0, 0, n - 1);
            return query(segment, left, right);
        }

        private int query(Segment segment, int left, int right) {
            if (left <= segment.start && right >= segment.end) return get(segment.index);
            if (segment.end < left || segment.start > right) return 0;
            int leftVal = query(segment.left(), left, right);
            int rightVal = query(segment.right(), left, right);
            return leftVal + rightVal;
        }

        static SegmentTree build(int[] arr) {
            int n = arr.length;
            var res = new SegmentTree(n);
            var segment = new Segment(0, 0, n - 1);
            res.build(arr, segment);
            return res;
        }

        private int build(int[] arr, Segment segment) {
            if (segment.isEmpty()) return set(segment.index, arr[segment.start]);
            var left = segment.left();
            var right = segment.right();
            int leftVal = set(left.index, build(arr, left));
            int rightVal = set(right.index, build(arr, right));
            return set(segment.index, leftVal + rightVal);
        }

        private static class Segment {
            int index;
            int start;
            int end;

            Segment(int index, int start, int end) {
                this.index = index;
                this.start = start;
                this.end = end;
            }

            int mid() {
                return (start + end) >> 1;
            }

            Segment left() {
                return new Segment((index << 1) + 1, start, mid());
            }

            Segment right() {
                return new Segment((index << 1) + 2, mid() + 1, end);
            }

            boolean isEmpty() {
                return start == end;
            }
        }
    }

    private final SegmentTree tree;

    public NumArray(int[] nums) {
        tree = SegmentTree.build(nums);
    }
 
    public int sumRange(int left, int right) {
        return tree.query(left, right);
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(left,right);
 */


/*
Given two arrays, write a function to compute their intersection.
*/
#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution {
public:
  vector<int> intersection(vector<int> &nums1, vector<int> &nums2) {
    vector<int> res;
    if (nums1.empty() || nums2.empty()) return res;
    unordered_set<int> c;
    for (int i : nums1) c.insert(i);
    for (int i : nums2) {
      if (c.erase(i) > 0) {
        res.push_back(i);
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  vector<int> v1{1,2,2,1};
  vector<int> v2{2,2};
  const vector<int> &res = sol.intersection(v1, v2);
  for (auto i : res) {
    cout << i << " ";
  } 
  cout << endl;
}

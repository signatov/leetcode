/*
https://leetcode.com/problems/design-hashmap/

706. Design HashMap

Design a HashMap without using any built-in hash table libraries.

Implement the MyHashMap class:

- MyHashMap() initializes the object with an empty map.
- void put(int key, int value) inserts a (key, value) pair into the HashMap. If the key already exists in the map, update the corresponding value.
- int get(int key) returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key.
- void remove(key) removes the key and its corresponding value if the map contains the mapping for the key.

Example 1:

Input
["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
[[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
Output
[null, null, null, 1, -1, null, 1, null, -1]

Explanation
MyHashMap myHashMap = new MyHashMap();
myHashMap.put(1, 1); // The map is now [[1,1]]
myHashMap.put(2, 2); // The map is now [[1,1], [2,2]]
myHashMap.get(1);    // return 1, The map is now [[1,1], [2,2]]
myHashMap.get(3);    // return -1 (i.e., not found), The map is now [[1,1], [2,2]]
myHashMap.put(2, 1); // The map is now [[1,1], [2,1]] (i.e., update the existing value)
myHashMap.get(2);    // return 1, The map is now [[1,1], [2,1]]
myHashMap.remove(2); // remove the mapping for 2, The map is now [[1,1]]
myHashMap.get(2);    // return -1 (i.e., not found), The map is now [[1,1]]

Constraints:

0 <= key, value <= 10^6
At most 10^4 calls will be made to put, get, and remove.
*/

import java.util.Optional;

class MyHashMap {
    private static class Entry {
        int key;
        Integer value;

        Entry(int key, Integer value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            return Objects.equals(key, ((Entry) o).key);
        }
    }

    private final List<Entry>[] buckets;

    public MyHashMap() {
        this(1 << 13);
    }

    public MyHashMap(int capacity) {
        this.buckets = new LinkedList[capacity];
    }
    
    public void put(int key, int value) {
        getEntry(key).ifPresentOrElse(e -> e.value = value, () -> getBucket(key).add(new Entry(key, value)));
    }
    
    public int get(int key) {
        return getEntry(key).map(e -> e.value).orElse(-1);
    }
    
    public void remove(int key) {
        getEntry(key).ifPresent(e -> getBucket(key).remove(e));
    }

    private Optional<Entry> getEntry(int key) {
        var bucket = getBucket(key);
        for (var e : bucket) {
            if (e.key == key) return Optional.of(e);
        }
        return Optional.empty();
    }

    private List<Entry> getBucket(int key) {
        int index = Integer.valueOf(key).hashCode() % buckets.length;
        var bucket = buckets[index];
        if (bucket == null) {
            bucket = buckets[index] = new LinkedList<>();
        }
        return bucket;
    }
}

/**
 * Your MyHashMap object will be instantiated and called as such:
 * MyHashMap obj = new MyHashMap();
 * obj.put(key,value);
 * int param_2 = obj.get(key);
 * obj.remove(key);
 */
 

/*
https://leetcode.com/problems/unique-paths-iii/

980. Unique Paths III

You are given an m x n integer array grid where grid[i][j] could be:

1 representing the starting square. There is exactly one starting square.
2 representing the ending square. There is exactly one ending square.
0 representing empty squares we can walk over.
-1 representing obstacles that we cannot walk over.
Return the number of 4-directional walks from the starting square to the ending square, that walk over every
non-obstacle square exactly once.

Example 1:

Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,2,-1]]
Output: 2
Explanation: We have the following two paths:
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)

Example 2:

Input: grid = [[1,0,0,0],[0,0,0,0],[0,0,0,2]]
Output: 4
Explanation: We have the following four paths:
1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)

Example 3:

Input: grid = [[0,1],[2,0]]
Output: 0
Explanation: There is no path that walks over every empty square exactly once.
Note that the starting and ending square can be anywhere in the grid.

Constraints:

m == grid.length
n == grid[i].length
1 <= m, n <= 20
1 <= m * n <= 20
-1 <= grid[i][j] <= 2
There is exactly one starting cell and one ending cell.
*/
class Solution {
    private val start = 1
    private val finish = 2
    private val wall = -1
    private val visited = 3

    private val dx = arrayOf(-1, 0, +1, 0)
    private val dy = arrayOf(0, +1, 0, -1)

    fun uniquePathsIII(grid: Array<IntArray>): Int {
        val m = grid.size
        val n = grid[0].size
        val (start, maxSteps) = findStartAndSteps(grid)

        fun visit(x: Int, y: Int, action: () -> Unit) {
            val old = grid[y][x]
            grid[y][x] = visited
            action()
            grid[y][x] = old
        }

        fun isReachable(x: Int, y: Int): Boolean =
            x in 0 until n && y in 0 until m

        fun isValid(x: Int, y: Int): Boolean =
            isReachable(x, y) && grid[y][x] != wall && grid[y][x] != visited

        fun findPaths(x: Int, y: Int, step: Int = 1): Int {
            if (grid[y][x] == finish)
                return if (step == maxSteps) 1 else 0
            var res = 0
            visit(x, y) {
                for (i in dx.indices) {
                    val nx = x + dx[i]
                    val ny = y + dy[i]
                    if (isValid(nx, ny))
                        res += findPaths(nx, ny, step + 1)
                }
            }
            return res
        }
        return findPaths(start.first, start.second)
    }

    private fun findStartAndSteps(grid: Array<IntArray>): Pair<Pair<Int, Int>, Int> {
        var s: Pair<Int, Int>? = null
        var maxSteps = 0
        for (i in grid.indices) {
            for (j in grid[0].indices) {
                if (grid[i][j] == start) s = Pair(j, i)
                if (grid[i][j] != wall) ++maxSteps
            }
        }
        return Pair(s!!, maxSteps)
    }
}

/*
https://leetcode.com/problems/number-of-islands/
Given an m x n 2d grid map of '1's (land) and '0's (water), return the number
of islands.

An island is surrounded by water and is formed by connecting adjacent lands
horizontally or vertically. You may assume all four edges of the grid are all
surrounded by water.

Example 1:
Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Example 2:
Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 300
grid[i][j] is '0' or '1'.
*/

#include <cassert>
#include <vector>
#include <queue>
using std::vector;

struct point2d {
  int x;
  int y;
  point2d() = default;
  constexpr point2d(int x, int y) : x(x), y(y) {}

  bool in_grid(size_t w, size_t h) const {
    return x >= 0 && y >= 0 && x < w && y < h;
  }

  point2d operator +(const point2d &step) {
    return {x + step.x, y + step.y};
  }
};

class Solution {
  static constexpr point2d STEPS[] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

  void bfs(int i, int j, vector<vector<bool>> & visited,
           const vector<vector<char>> & grid) {
    std::queue<point2d> queue;
    queue.emplace(i, j);

    size_t m{grid.size()};
    size_t n{grid.at(0).size()};

    while (!queue.empty()) {
      auto p{queue.front()};
      queue.pop();
      for (auto step : STEPS) {
        auto np{p + step};
        if (np.in_grid(m, n) && grid[np.x][np.y] == '1' && !visited[np.x][np.y]) {
          visited[np.x][np.y] = true;
          queue.push(std::move(np));
        }
      }
    }
  }
public:
  int numIslands(vector<vector<char>> & grid) {
    if (grid.empty() || grid.at(0).empty()) return 0;
    int result{0};
    size_t m{grid.size()};
    size_t n{grid.at(0).size()};
    vector<vector<bool>> visited{m, vector<bool>(n)};
    for (int i = 0; i < m; ++i) {
      for (int j = 0; j < n; ++j) {
        if (grid[i][j] == '1' && !visited[i][j]) {
          ++result;
          bfs(i, j, visited, grid);
        }
      }
    }
    return result;
  }
};

int main() {
  vector<vector<char>> v = {
    {'1','1','0','0','0'},
    {'1','1','0','0','0'},
    {'0','0','1','0','0'},
    {'0','0','0','1','1'}
  };
  Solution sol;
  assert(sol.numIslands(v) == 3);
}

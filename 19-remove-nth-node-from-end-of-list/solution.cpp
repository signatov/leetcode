/*
https://leetcode.com/problems/remove-nth-node-from-end-of-list

Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.

Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?
*/
#include <cassert>
#include <iostream>
#include <queue>
#include "../listnode.h"

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
  ListNode* removeNthFromEnd(ListNode* head, int n) {
    if (n < 1) return head;
    if (n == 1 && !head->next) return NULL;
    queue<ListNode *> q;
    for (ListNode *t = head; t; t = t->next) {
      if (q.size() <= n) {
        q.push(t);
      } else {
        q.pop();
        q.push(t);
      }
    }
    if (q.size() > n) {
      ListNode *p = q.front();
      p->next = p->next ? p->next->next : NULL;
    } else if (q.size() == n) {
      head = q.front()->next;
    }
    return head;
  }
};

void check1() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{1,2,3,4,5}), &deleteList};
  ListNode *h = sol.removeNthFromEnd(l1.get(), 2);
  assert(h && h->val == 1);
  assert(h->next && h->next->val == 2);
  assert(h->next->next && h->next->next->val == 3);
  assert(h->next->next->next && h->next->next->next->val == 5);
}

void check2() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{1,2}), &deleteList};
  ListNode *h = sol.removeNthFromEnd(l1.get(), 1);
  assert(h && h->val == 1);
  assert(!h->next);
}

void check3() {
  Solution sol;
  ListPtr l1{makeList(initializer_list<int>{1,2}), &deleteList};
  ListNode *h = sol.removeNthFromEnd(l1.get(), 2);
  assert(h && h->val == 2);
  assert(!h->next);
}

int main() {
  check1();
  check2();
  check3();
}


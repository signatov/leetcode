/*
A self-dividing number is a number that is divisible by every digit it contains.

For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

Also, a self-dividing number is not allowed to contain the digit zero.

Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.
*/
#include <iostream>
#include <vector>

using namespace std;

class Solution {
  static vector<int> getDigits(int i) {
    vector<int> res;
    while (i > 0) {
      res.push_back(i % 10);
      i /= 10;
    }
    return res;
  }
  static void inc(vector<int> &v) {
    int i = 0;
    for (;;) {
      v[i] = v[i] + 1;
      if (v[i] < 10) break;
      v[i++] = 0;
      if (i >= v.size()) {
        v.push_back(1);
        break;
      }
    }
  }
  static bool allDividing(int value, const vector<int> &v) {
    for (int i : v) {
      if (i == 0) return false;
      if (value % i) return false;
    }
    return true;
  }
public:
  vector<int> selfDividingNumbers(int left, int right) {
    vector<int> res;
    res.reserve((right - left) / 2);

    vector<int> v = getDigits(left);
    for (int i = left; i <= right; ++i, inc(v)) {
      if (allDividing(i, v)) {
        res.push_back(i);
      }
    }

    return res;
  }
};

int main() {
  Solution sol;
  vector<int> v = sol.selfDividingNumbers(1, 22);
  for (int i : v) { cout << i << " "; }
  cout << endl;
}

/*
https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

108. Convert Sorted Array to Binary Search Tree

Given an integer array nums where the elements are sorted in ascending order, convert it to a height-balanced binary
search tree.

Example 1:

Input: nums = [-10,-3,0,5,9]
Output: [0,-3,9,-10,null,5]
Explanation: [0,-10,5,null,-3,null,9] is also accepted

Example 2:

Input: nums = [1,3]
Output: [3,1]
Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.

Constraints:

1 <= nums.length <= 10^4
-10^4 <= nums[i] <= 10^4
nums is sorted in a strictly increasing order.
*/
/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class Solution {
    private fun convert(nums: IntArray, r: IntRange): TreeNode? {
        if (r.isEmpty()) return null
        val i = (r.first + r.last) / 2
        val n = TreeNode(nums[i])
        n.left = convert(nums, IntRange(r.first, i - 1))
        n.right = convert(nums, IntRange(i + 1, r.last))
        return n
    }

    fun sortedArrayToBST(nums: IntArray): TreeNode? {
        if (nums.size == 1) return TreeNode(nums[0])
        val i = nums.size / 2
        val root = TreeNode(nums[i])
        root.left = convert(nums, IntRange(0, i - 1))
        root.right = convert(nums, IntRange(i + 1, nums.size - 1))
        return root
    }
}

from collections import Counter

class Solution:
    def permuteUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        count = Counter(nums)
        res = []

        def permute(n, l):
            if n == 0:
                res.append([i for i in l])
            else:
                for i in count:
                    if count[i] > 0:
                        l.append(i)
                        count[i] -= 1
                        permute(n - 1, l)
                        count[i] += 1
                        del l[-1]

        permute(len(nums), [])

        return res

package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPermutateUnique(t *testing.T) {
	a := []int{1, 1, 2}
	assert.Equal(t, len(permuteUnique(a)), 3, "Test#1 failed")
	a = []int{1, 1, 2, 3}
	assert.Equal(t, len(permuteUnique(a)), 12, "Test#2 failed")
}

package solution

// https://leetcode.com/problems/permutations-ii

// Given a collection of numbers that might contain duplicates, return all possible unique permutations.

// Example:
// Input: [1,1,2]
// Output:
// [
//   [1,1,2],
//   [1,2,1],
//   [2,1,1]
// ]

func permuteUnique(nums []int) [][]int {
	if len(nums) == 0 {
		return make([][]int, 0)
	}

	makeCopy := func(a []int) []int {
		t := make([]int, len(a))
		copy(t, a)
		return t
	}

	type resultFunc func(a []int)

	var impl func(int, []int, resultFunc)

	uniq := []int{}
	count := []int{}

	impl = func(n int, aux []int, onResult resultFunc) {
		if n == 0 {
			onResult(makeCopy(aux))
		} else {
			for i := range uniq {
				if count[i] > 0 {
					aux = append(aux, uniq[i])
					count[i]--
					impl(n-1, aux, onResult)
					count[i]++
					aux = aux[:len(aux)-1]
				}
			}
		}
	}

	res := [][]int{}
	store := func(a []int) {
		res = append(res, a)
	}
	freq := map[int]int{}
	for i := range nums {
		_, ok := freq[nums[i]]
		if !ok {
			freq[nums[i]] = 1
		} else {
			freq[nums[i]]++
		}
	}
	for i := range freq {
		uniq = append(uniq, i)
		count = append(count, freq[i])
	}
	impl(len(nums), []int{}, store)
	return res
}

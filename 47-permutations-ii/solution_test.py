import unittest
import solution

class TestSolution(unittest.TestCase):
    def setUp(self):
        self.sol = solution.Solution()

    def test_case1(self):
        self.assertTrue(len(self.sol.permuteUnique([1,1,2])) == 3)

    def test_case2(self):
        self.assertEqual(len(self.sol.permuteUnique([1,1,2,3])), 12)

if __name__ == '__main__':
    unittest.main()

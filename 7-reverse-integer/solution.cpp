/*
Given a 32-bit signed integer, reverse digits of an integer.
*/
#include <iostream>
#include <cstdlib>
#include <climits>

using namespace std;

class Solution {
  int nextDigit(int input, int output) {
    if (input == 0) return output;
    if (input > 0 && ((output > INT_MAX / 10) || (input % 10) > (INT_MAX - output * 10)))
      return 0; // integer overflow
    if (input < 0 && ((output < INT_MIN / 10) || (input % 10) < (INT_MIN - output * 10)))
      return 0; // integer underflow
    return nextDigit(input / 10, output * 10 + input % 10);
  }
public:
  int reverse(int x) {
    if (abs(x) < 10) return x;
    return nextDigit(x, 0);
  }
};

int main() {
  Solution sol;
  cout << sol.reverse(123) << endl;
  cout << sol.reverse(0) << endl;
  cout << sol.reverse(-123) << endl;
  cout << sol.reverse(120) << endl;
  cout << sol.reverse(1534236469) << endl;
  cout << sol.reverse(-2147483648) << endl;
}


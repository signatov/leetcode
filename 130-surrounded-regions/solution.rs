/*
https://leetcode.com/problems/surrounded-regions

130. Surrounded Regions

You are given an m x n matrix board containing letters 'X' and 'O', capture regions that are surrounded:

Connect: A cell is connected to adjacent cells horizontally or vertically.
Region: To form a region connect every 'O' cell.
Surround: The region is surrounded with 'X' cells if you can connect the region with 'X' cells and none of the region
cells are on the edge of the board.
A surrounded region is captured by replacing all 'O's with 'X's in the input matrix board.

Example 1:

Input: board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]

Output: [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]

In the above diagram, the bottom region is not captured because it is on the edge of the board and cannot be surrounded.

Example 2:

Input: board = [["X"]]

Output: [["X"]]

Constraints:

m == board.length
n == board[i].length
1 <= m, n <= 200
board[i][j] is 'X' or 'O'.
*/
impl Solution {
    pub fn solve(board: &mut Vec<Vec<char>>) {
        const FREE: char = 'O';
        const BUSY: char = 'X';
        const VISITED: char = 'V';

        let n = board.len();
        let m = board[0].len();

        fn traversal(board: &mut Vec<Vec<char>>, i: i32, j: i32) {
            const DELTA: [[i32; 4]; 2] = [[1, 0, -1, 0], [0, 1, 0, -1]];

            let n = board.len() as i32;
            let m = board[0].len() as i32;

            board[i as usize][j as usize] = VISITED;
            for k in 0..DELTA[0].len() {
                let ni = i + DELTA[0][k];
                let nj = j + DELTA[1][k];
                if ni >= 0
                    && ni < n
                    && nj >= 0
                    && nj < m
                    && board[ni as usize][nj as usize] == FREE
                {
                    traversal(board, ni, nj);
                }
            }
        }

        for i in 0..n {
            let b: [usize; 2] = [0, m - 1];
            for j in b {
                if board[i][j] == FREE {
                    traversal(board, i as i32, j as i32);
                }
            }
        }
        for j in 0..m {
            let b: [usize; 2] = [0, n - 1];
            for i in b {
                if board[i][j] == FREE {
                    traversal(board, i as i32, j as i32);
                }
            }
        }

        for i in 0..n {
            for j in 0..m {
                board[i][j] = if board[i][j] == VISITED { FREE } else { BUSY }
            }
        }
    }
}

/*
https://leetcode.com/problems/search-a-2d-matrix/

74. Search a 2D Matrix

You are given an m x n integer matrix matrix with the following two properties:

Each row is sorted in non-decreasing order.
The first integer of each row is greater than the last integer of the previous row.
Given an integer target, return true if target is in matrix or false otherwise.

You must write a solution in O(log(m * n)) time complexity.

Example 1:

Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
Output: true

Example 2:

Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
Output: false

Constraints:

m == matrix.length
n == matrix[i].length
1 <= m, n <= 100
-10^4 <= matrix[i][j], target <= 10^4
*/
class Solution {
    fun searchMatrix(matrix: Array<IntArray>, target: Int): Boolean {
        val m = matrix.size
        val n = matrix[0].size
        val size = n * m
        var l = 0
        var r = size - 1
        while (l <= r) {
            val mid = (l + r) / 2
            val p = getCoord(mid, n)
            val v = matrix[p.first][p.second]
            when {
                v == target -> return true
                v > target -> r = mid - 1
                else -> l = mid + 1
            }
        }
        return false
    }

    private fun getCoord(i: Int, n: Int): Pair<Int, Int> {
        val r = i / n
        val c = i - n * r
        return Pair(r, c)
    }
}

/*
https://leetcode.com/problems/valid-number

Validate if a given string is numeric.

Some examples:
"0" => true
" 0.1 " => true
"abc" => false
"1 a" => false
"2e10" => true

Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.
*/
#include <cassert>
#include <cctype>
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static inline const char * skipSpaces(const char * p) {
    for (; *p && isspace(*p); ++p);
    return p;
  }
  bool isNumberImpl(string s, int level = 0, bool withDots = true) {
    if (level > 1 || s.empty() || s.find_first_not_of("+-.Ee0123456789 \t\n\v\f\r") != string::npos)
      return false;

    const char * p = skipSpaces(s.c_str());
    if (!*p) return false;
    if (*p == '+' || *p == '-') ++p;
    bool anyDigits = false;
    bool hasDot = false;
    for (; *p; ++p) {
      if (isspace(*p)) break;
      if (isdigit(*p)) anyDigits = true;
      else if (*p == '.') {
        if (hasDot || !withDots) return false;
        hasDot = true;
      } else if (*p == 'E' || *p == 'e') {
        return isNumberImpl(string(s.c_str(), p - s.c_str()), level + 1) && isNumberImpl(string(p + 1), level + 1, false);
      } else { // '+' or '-'
        return false;
      }
    }
    if (!anyDigits) return false;
    if (!*p) return true;
    p = skipSpaces(p);
    return !*p;
  }
public:
  bool isNumber(string s) {
    return isNumberImpl(s);
  }
};

int main() {
  Solution sol;
  assert(!sol.isNumber("    "));
  assert(!sol.isNumber(""));
  assert(sol.isNumber("0"));
  assert(sol.isNumber(" 0.1 "));
  assert(!sol.isNumber("abc"));
  assert(!sol.isNumber("1 a"));
  assert(sol.isNumber("2e10"));
  assert(sol.isNumber("2E10"));
  assert(!sol.isNumber("2.2E10.2"));
  assert(sol.isNumber("79867565686578970870870879786564545211212"));
  assert(sol.isNumber(".1"));
  assert(!sol.isNumber("1.1.1"));
  assert(sol.isNumber(" -.1"));
  assert(!sol.isNumber("."));
  assert(!sol.isNumber("-"));
  assert(!sol.isNumber("e"));
  assert(!sol.isNumber("-e"));
  assert(!sol.isNumber("1 2"));
  assert(!sol.isNumber(". 1"));
  assert(!sol.isNumber("6-1"));
  assert(!sol.isNumber("6+1"));
  assert(!sol.isNumber("3.5e+3.5e+3.5"));
}


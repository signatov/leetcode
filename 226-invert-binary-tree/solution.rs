/*
https://leetcode.com/problems/invert-binary-tree/

226. Invert Binary Tree

Given the root of a binary tree, invert the tree, and return its root.

Example 1:

Input: root = [4,2,7,1,3,6,9]
Output: [4,7,2,9,6,3,1]

Example 2:

Input: root = [2,1,3]
Output: [2,3,1]

Example 3:

Input: root = []
Output: []

Constraints:

The number of nodes in the tree is in the range [0, 100].
-100 <= Node.val <= 100
*/
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::rc::Rc;
use std::cell::RefCell;

type MaybeNode = Option<Rc<RefCell<TreeNode>>>;

impl Solution {
  pub fn invert_tree(root: MaybeNode) -> MaybeNode {
    match root {
      None => root,
      Some(r) => {
        let r = r.as_ref().borrow();
        let mut root = TreeNode::new(r.val);
        root.left = Self::invert_tree(r.right.clone());
        root.right = Self::invert_tree(r.left.clone());
        Some(Rc::new(RefCell::new(root)))
      }
    }
  }
}

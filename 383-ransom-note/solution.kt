/*
https://leetcode.com/problems/ransom-note/

383. Ransom Note

Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from
magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.

Example 1:

Input: ransomNote = "a", magazine = "b"
Output: false

Example 2:

Input: ransomNote = "aa", magazine = "ab"
Output: false

Example 3:

Input: ransomNote = "aa", magazine = "aab"
Output: true

Constraints:

1 <= ransomNote.length, magazine.length <= 10^5
ransomNote and magazine consist of lowercase English letters.
*/
class Solution {
    private val size = 'z' - 'a' + 1
    fun canConstruct(ransomNote: String, magazine: String): Boolean {
        if (magazine == ransomNote) return true
        val letters = IntArray(size)
        for (c in magazine) letters[c - 'a'] = letters[c - 'a'] + 1
        for (c in ransomNote) {
            val i = c - 'a'
            letters[i] = letters[i] - 1
            if (letters[i] < 0) return false
        }
        return true
    }
}

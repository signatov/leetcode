package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearchInsert(t *testing.T) {
	a := []int{1, 3, 5, 6}
	assert.Equal(t, searchInsert(a, 5), 2, "Test#1 failed")
	assert.Equal(t, searchInsert(a, 2), 1, "Test#2 failed")
	assert.Equal(t, searchInsert(a, 7), 4, "Test#3 failed")
	assert.Equal(t, searchInsert(a, 0), 0, "Test#4 failed")
	a = []int{1}
	assert.Equal(t, searchInsert(a, 0), 0, "Test#5 failed")
	assert.Equal(t, searchInsert(a, 2), 1, "Test#6 failed")
	a = []int{1, 3}
	assert.Equal(t, searchInsert(a, 3), 1, "Test#7 failed")
	a = []int{1, 3, 5}
	assert.Equal(t, searchInsert(a, 1), 0, "Test#8 failed")
}

package solution

// https://leetcode.com/problems/search-insert-position

// Given a sorted array and a target value, return the index if the target is found.
// If not, return the index where it would be if it were inserted in order.

// You may assume no duplicates in the array.

// Example 1:
// Input: [1,3,5,6], 5
// Output: 2

// Example 2:
// Input: [1,3,5,6], 2
// Output: 1

// Example 3:
// Input: [1,3,5,6], 7
// Output: 4

// Example 4:
// Input: [1,3,5,6], 0
// Output: 0

func searchInsert(nums []int, target int) int {
	if len(nums) == 0 {
		return 0
	}

	lo := 0
	hi := len(nums) - 1
	if hi == lo {
		if nums[lo] < target {
			return hi + 1
		}
		return lo
	}
	mid := (lo + hi) / 2
	for lo < hi {
		if target > nums[mid] {
			lo = mid
		} else if target < nums[mid] {
			hi = mid
		} else {
			break
		}
		if lo == hi-1 {
			if target == nums[lo] {
				return lo
			} else if target > nums[lo] && target < nums[hi] {
				return hi
			} else if target == nums[hi] {
				return hi
			} else if target > nums[hi] {
				return hi + 1
			}
		}
		mid = (lo + hi) / 2
	}

	return mid
}

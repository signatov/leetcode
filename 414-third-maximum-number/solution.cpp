/*
Given a non-empty array of integers, return the third maximum number in this array. If it does not exist, return the maximum number.
The time complexity must be in O(n).
*/
#include <iostream>
#include <vector>
#include <climits>
#include <stdexcept>

using namespace std;

class Solution {
  int max1{INT_MIN};
  int max2{INT_MIN};
  int max3{INT_MIN};

  bool max1Updated{false};  // std::optional only since C++17.
  bool max2Updated{false};
  bool max3Updated{false};

  void reset() {
    max1 = max2 = max3 = INT_MIN;
    max1Updated = max2Updated = max3Updated = false;
  }

  int thirdOrMax() {
    return max3Updated ? max3 : max1;
  }

  void update1(int v) {
    if (max1Updated && v > max1) update2(max1);
    max1 = v;
    max1Updated = true;
  }

  void update2(int v) {
    if (max2Updated && v > max2) update3(max2);
    max2 = v;
    max2Updated = true;
  }

  void update3(int v) {
    max3 = v;
    max3Updated = true;
  }

public:
  int thirdMax(vector<int> &nums) {
    if (nums.empty()) throw new domain_error("Input should be non-empty array");
    reset();
    for (auto v : nums) {
      if (v >= max1 || !max1Updated) { update1(v); }
      else if (v >= max2 || !max2Updated) { update2(v); }
      else if (v >= max3 || !max3Updated) { update3(v); }
    }

    return thirdOrMax();
  } 
};

int main() {
  Solution sol;
  vector<int> v1{3,2,1};
  vector<int> v2{1,2};
  vector<int> v3{2,2,3,1};
  vector<int> v4{5,2,2};
  vector<int> v5{2,2,3,1};
  vector<int> v6{1,2,2,5,3,5};
  vector<int> v7{1,-2147483648,2};
  cout << sol.thirdMax(v1) << endl;
  cout << sol.thirdMax(v2) << endl;
  cout << sol.thirdMax(v3) << endl;
  cout << sol.thirdMax(v4) << endl;
  cout << sol.thirdMax(v5) << endl;
  cout << sol.thirdMax(v6) << endl;
  cout << sol.thirdMax(v7) << endl;
}


/*
https://leetcode.com/problems/majority-element/

169. Majority Element

Given an array nums of size n, return the majority element.

The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element
always exists in the array.

Example 1:

Input: nums = [3,2,3]
Output: 3

Example 2:

Input: nums = [2,2,1,1,1,2,2]
Output: 2

Constraints:

n == nums.length
1 <= n <= 5 * 10^4
-10^9 <= nums[i] <= 10^9

Follow-up: Could you solve the problem in linear time and in O(1) space?
*/
use std::collections::HashMap;

impl Solution {
  pub fn majority_element(nums: Vec<i32>) -> i32 {
    let m = nums.len() / 2;
    let mut counts = HashMap::new();
    for v in nums.iter() {
      if let Some(x) = counts.get_mut(&v) {
        *x = *x + 1;
      } else {
          counts.insert(v, 1 as usize);
      }
    }
    for (i, c) in &counts {
      if *c > m {
        return **i;
      }
    }
    0
  }
}

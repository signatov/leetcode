package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPermutate(t *testing.T) {
	a := []int{1, 2, 3}
	assert.Equal(t, len(permute(a)), 6, "Test#1 failed")
	a = []int{1, 2, 3, 4}
	assert.Equal(t, len(permute(a)), 24, "Test#1 failed")
}

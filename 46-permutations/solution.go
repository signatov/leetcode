package solution

// https://leetcode.com/problems/permutations

// Given a collection of distinct integers, return all possible permutations.

// Example:
// Input: [1,2,3]
// Output:
// [
//   [1,2,3],
//   [1,3,2],
//   [2,1,3],
//   [2,3,1],
//   [3,1,2],
//   [3,2,1]
// ]

func permute(nums []int) [][]int {
	if len(nums) == 0 {
		return make([][]int, 0)
	}

	swap := func(a []int, i int, j int) {
		if i != j {
			t := a[i]
			a[i] = a[j]
			a[j] = t
		}
	}

	makeCopy := func(a []int) []int {
		t := make([]int, len(a))
		copy(t, a)
		return t
	}

	type resultFunc func(a []int)

	var impl func([]int, int, resultFunc)

	impl = func(a []int, n int, onResult resultFunc) {
		if n == 1 {
			onResult(makeCopy(a))
		} else {
			for i := 0; i < n; i++ {
				impl(a, n-1, onResult)
				if n%2 == 1 {
					swap(a, i, n-1)
				} else {
					swap(a, 0, n-1)
				}
			}
		}
	}

	res := [][]int{}
	store := func(a []int) {
		res = append(res, a)
	}
	a := makeCopy(nums)
	impl(a, len(a), store)
	return res
}

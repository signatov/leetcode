/*
https://leetcode.com/problems/word-search/

79. Word Search

Given an m x n grid of characters board and a string word, return true if word exists in the grid.

The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or
vertically neighboring. The same letter cell may not be used more than once.

Example 1:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
Output: true

Example 2:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
Output: true

Example 3:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
Output: false

Constraints:

m == board.length
n = board[i].length
1 <= m, n <= 6
1 <= word.length <= 15
board and word consists of only lowercase and uppercase English letters.
*/
class Solution {
    companion object {
        private const val VISITED = '\u0000'
        private val DELTAS = arrayOf(
            Pos(-1, 0), Pos(1, 0), Pos(0, -1), Pos(0, 1))
    }

    fun exist(board: Array<CharArray>, word: String): Boolean {
        val b = Board(board)
        val len = word.length

        fun search(p: Pos, index: Int): Boolean {
            if (len == index) return true
            val n = b.neighbors(p)
            val c = word[index]
            var res = false
            for (i in n) {
                if (b[i] == c) {
                    b[i] = VISITED
                    res = res or search(i, index + 1)
                    b[i] = c
                }
            }
            return res
        }

        val positions = b.findChars(word[0])
        for (p in positions) {
            val c = b[p]
            b[p] = VISITED
            if (search(p, 1))
                return true
            b[p] = c
        }
        return false
    }

    class Board(private val board: Array<CharArray>) {
        private val height = board.size - 1
        private val width = board[0].size - 1

        operator fun get(p: Pos): Char = board[p.y][p.x]

        operator fun set(p: Pos, v: Char) {
            board[p.y][p.x] = v
        }

        fun findChars(c: Char) = sequence {
            for (i in board.indices) {
                for (j in board[i].indices) {
                    if (board[i][j] == c) {
                        yield(Pos(i, j))
                    }
                }
            }
        }

        fun neighbors(p: Pos) = sequence {
            for (i in DELTAS) {
                val n = p + i
                if (n.y in 0..height && n.x in 0..width)
                    yield(n)
            }
        }
    }

    data class Pos(val y: Int, val x: Int) {
        operator fun plus(d: Pos) = Pos(y + d.y, x + d.x)
    }
}

package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemoveDuplicates(t *testing.T) {
	a := []int{1, 1, 2}
	assert.Equal(t, removeDuplicates(a), 2, "Test#1 failed")
	assert.Equal(t, a[1], 2, "Test#1 failed")
	a = []int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}
	assert.Equal(t, removeDuplicates(a), 5, "Test#1 failed")
	a = []int{1, 1}
	assert.Equal(t, removeDuplicates(a), 1, "Test#1 failed")
}

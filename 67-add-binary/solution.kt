/*
https://leetcode.com/problems/add-binary/

67. Add Binary

Given two binary strings a and b, return their sum as a binary string.

Example 1:

Input: a = "11", b = "1"
Output: "100"

Example 2:

Input: a = "1010", b = "1011"
Output: "10101"

Constraints:

1 <= a.length, b.length <= 10^4
a and b consist only of '0' or '1' characters.
Each string does not contain leading zeros except for the zero itself.
*/
class Solution {
    fun addBinary(a: String, b: String): String {
        val res = StringBuilder()
        var i = a.length - 1
        var j = b.length - 1
        var c = 0
        while (i >= 0 || j >= 0) {
            var s = c
            if (i >= 0) s += a[i--] - '0'
            if (j >= 0) s += b[j--] - '0'
            c = when (s) {
                3 -> {
                    res.insert(0, 1)
                    1
                }
                2 -> {
                    res.insert(0, 0)
                    1
                }
                else -> {
                    res.insert(0, s)
                    0
                }
            }
        }
        if (c > 0) res.insert(0, c)
        return res.toString()
    }
}

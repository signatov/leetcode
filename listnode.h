#ifndef __LIST_NODE_H__
#define __LIST_NODE_H__

#include <initializer_list>
#include <iostream>
#include <memory>

struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};

ListNode *makeList(std::initializer_list<int> l) {
  ListNode *res = NULL, *tail = NULL;
  for (auto i = l.begin(); i != l.end(); ++i) {
    ListNode *node = new ListNode(*i);
    if (!res) {
      res = tail = node;
    } else {
      tail->next = node;
      tail = node;
    }
  }
  return res;
}

void deleteList(ListNode * list) {
  while (list) {
    ListNode * t = list;
    list = list->next;
    delete t;
  }
}

void printList(ListNode *l) {
  for (; l; l = l->next) {
    std::cout << l->val << (l->next ? "," : "");
  }
  std::cout << std::endl;
}

using ListPtr = std::unique_ptr<ListNode, decltype(&deleteList)>;

#endif //__LIST_NODE_H__


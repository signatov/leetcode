package solution

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValidSudoku(t *testing.T) {
	a := [][]byte{}
	row1 := []byte{'8', '3', '.', '.', '7', '.', '.', '.', '.'}
	row2 := []byte{'6', '.', '.', '1', '9', '5', '.', '.', '.'}
	row3 := []byte{'.', '9', '8', '.', '.', '.', '.', '6', '.'}
	row4 := []byte{'8', '.', '.', '.', '6', '.', '.', '.', '3'}
	row5 := []byte{'4', '.', '.', '8', '.', '3', '.', '.', '1'}
	row6 := []byte{'7', '.', '.', '.', '2', '.', '.', '.', '6'}
	row7 := []byte{'.', '6', '.', '.', '.', '.', '2', '8', '.'}
	row8 := []byte{'.', '.', '.', '4', '1', '9', '.', '.', '5'}
	row9 := []byte{'.', '.', '.', '.', '8', '.', '.', '7', '9'}

	a = append(a, row1)
	a = append(a, row2)
	a = append(a, row3)
	a = append(a, row4)
	a = append(a, row5)
	a = append(a, row6)
	a = append(a, row7)
	a = append(a, row8)
	a = append(a, row9)

	assert.Equal(t, isValidSudoku(a), false, "Test#1 failed")
}

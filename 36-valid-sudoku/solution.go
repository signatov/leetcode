package solution

// https://leetcode.com/problems/valid-sudoku

// Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

// 1. Each row must contain the digits 1-9 without repetition.
// 2. Each column must contain the digits 1-9 without repetition.
// 3. Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.

func getSubgrid(board [][]byte, row0 int, row1 int, col0 int, col1 int) []byte {
	sequence := make([]byte, 0)
	for i := row0; i < row1; i++ {
		for j := col0; j < col1; j++ {
			sequence = append(sequence, board[i][j])
		}
	}
	return sequence
}

func getColumn(board [][]byte, index int) []byte {
	sequence := make([]byte, 0)
	for _, row := range board {
		sequence = append(sequence, row[index])
	}
	return sequence
}

func isValidSequence(sequence []byte) bool {
	digits := map[byte]bool{
		'1': true, '2': true, '3': true,
		'4': true, '5': true, '6': true,
		'7': true, '8': true, '9': true}
	for i := range sequence {
		if sequence[i] != '.' {
			_, ok := digits[sequence[i]]
			if ok {
				delete(digits, sequence[i])
			} else {
				return false
			}
		}
	}
	return true
}

func isValidSudoku(board [][]byte) bool {
	for i := 0; i < len(board); i++ {
		if !isValidSequence(board[i]) {
			return false
		}
	}
	for i := 0; i < len(board); i++ {
		if !isValidSequence(getColumn(board, i)) {
			return false
		}
	}
	for i := 0; i < 9; i = i + 3 {
		for j := 0; j < 9; j = j + 3 {
			if !isValidSequence(getSubgrid(board, i, i+3, j, j+3)) {
				return false
			}
		}
	}

	return true
}

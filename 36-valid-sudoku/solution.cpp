/*
https://leetcode.com/problems/valid-sudoku

Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

1. Each row must contain the digits 1-9 without repetition.
2. Each column must contain the digits 1-9 without repetition.
3. Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.
*/
#include <cassert>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution {
public:
  bool isValidSudoku(vector<vector<char>>& board) {
    unordered_set<char> rows[9];
    unordered_set<char> columns[9];
    unordered_set<char> blocks[3][3];

    for (size_t i = 0; i < board.size(); ++i) {
      for (size_t j = 0; j < board[i].size(); ++j) {
        char c = board[i][j];
        if (c == '.') continue;

        if (rows[i].count(c) > 0) return false;
        if (columns[j].count(c) > 0) return false;
        if (blocks[i / 3][j / 3].count(c) > 0) return false;

        rows[i].insert(c);
        columns[j].insert(c);
        blocks[i / 3][j / 3].insert(c);
      }
    }
    return true;
  }
};

int main() {
  Solution sol;
  vector<vector<char>> board{
    {'5','3','.','.','7','.','.','.','.'},
    {'6','.','.','1','9','5','.','.','.'},
    {'.','9','8','.','.','.','.','6','.'},
    {'8','.','.','.','6','.','.','.','3'},
    {'4','.','.','8','.','3','.','.','1'},
    {'7','.','.','.','2','.','.','.','6'},
    {'.','6','.','.','.','.','2','8','.'},
    {'.','.','.','4','1','9','.','.','5'},
    {'.','.','.','.','8','.','.','7','9'}
  };
  assert(sol.isValidSudoku(board));
}

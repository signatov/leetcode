/*
https://leetcode.com/problems/edit-distance

Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

You have the following 3 operations permitted on a word:

- Insert a character
- Delete a character
- Replace a character

Example 1:
Input: word1 = "horse", word2 = "ros"
Output: 3
Explanation: 
horse -> rorse (replace 'h' with 'r')
rorse -> rose (remove 'r')
rose -> ros (remove 'e')

Example 2:
Input: word1 = "intention", word2 = "execution"
Output: 5
Explanation: 
intention -> inention (remove 't')
inention -> enention (replace 'i' with 'e')
enention -> exention (replace 'n' with 'x')
exention -> exection (replace 'n' with 'c')
exection -> execution (insert 'u')
*/
#include <algorithm>
#include <cassert>
#include <string>
#include <vector>

using namespace std;

class Solution {
  static int levenshtein(const string & s1, const string & s2) {
    const size_t M = s1.size();
    const size_t N = s2.size();
    vector<vector<int>> D(M + 1, vector<int>(N + 1));
    D[0][0] = 0;
    for (size_t i = 1; i <= M; ++i) D[i][0] = i;
    for (size_t i = 1; i <= N; ++i) D[0][i] = i;
    for (size_t i = 1; i <= M; ++i) {
      for (size_t j = 1; j <= N; ++j) {
        int matched = s1[i - 1] == s2[j - 1] ? 0 : 1;
        D[i][j] = min({D[i - 1][j] + 1, D[i][j - 1] + 1, D[i - 1][j - 1] + matched});
      }
    }
    return D[M][N];
  }
public:
  int minDistance(string word1, string word2) {
    if (word1 == word2)
      return 0;

    if (word1.empty() || word2.empty())
      return max(word1.size(), word2.size());

    return levenshtein(word1, word2);
  }
};

int main() {
  Solution sol;
  assert(sol.minDistance("a", "a") == 0);
  assert(sol.minDistance("b", "a") == 1);
  assert(sol.minDistance("", "a") == 1);
  assert(sol.minDistance("", "") == 0);
  assert(sol.minDistance("abc", "abac") == 1);
  assert(sol.minDistance("aba", "abc") == 1);
  assert(sol.minDistance("intention", "execution") == 5);
  assert(sol.minDistance("horse", "ros") == 3);
}


/*
https://leetcode.com/problems/symmetric-tree/

101. Symmetric Tree

Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

Example 1:

Input: root = [1,2,2,3,4,4,3]
Output: true

Example 2:

Constraints:

The number of nodes in the tree is in the range [1, 1000].
-100 <= Node.val <= 100

Follow up: Could you solve it both recursively and iteratively?
*/
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::rc::Rc;
use std::cell::RefCell;

type MaybeNode = Option<Rc<RefCell<TreeNode>>>;

impl Solution {
  pub fn is_symmetric(root: MaybeNode) -> bool {
    match root {
      None => true,
      Some(root) => {
        fn is_symmetric(left: &MaybeNode, right: &MaybeNode) -> bool {
          match (left, right) {
            (None, None) => true,
            (None, _) | (_, None) => false,
            (Some(l), Some(r)) if l.borrow().val != r.borrow().val => false,
            (Some(l), Some(r)) => {
              is_symmetric(&l.borrow().left, &r.borrow().right) &&
              is_symmetric(&l.borrow().right, &r.borrow().left)
            }
          }
        }
        let root = root.borrow();
        is_symmetric(&root.left, &root.right)
      }
    }
  }
}

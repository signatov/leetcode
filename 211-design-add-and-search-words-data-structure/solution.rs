/*
https://leetcode.com/problems/design-add-and-search-words-data-structure

211. Design Add and Search Words Data Structure

Design a data structure that supports adding new words and finding if a string matches any previously added string.

Implement the WordDictionary class:

- WordDictionary() Initializes the object.
- void addWord(word) Adds word to the data structure, it can be matched later.
- bool search(word) Returns true if there is any string in the data structure that matches word or false otherwise.
word may contain dots '.' where dots can be matched with any letter.

Example:

Input
["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
[[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
Output
[null,null,null,null,false,true,true,true]

Explanation
WordDictionary wordDictionary = new WordDictionary();
wordDictionary.addWord("bad");
wordDictionary.addWord("dad");
wordDictionary.addWord("mad");
wordDictionary.search("pad"); // return False
wordDictionary.search("bad"); // return True
wordDictionary.search(".ad"); // return True
wordDictionary.search("b.."); // return True

Constraints:

1 <= word.length <= 25
word in addWord consists of lowercase English letters.
word in search consist of '.' or lowercase English letters.
There will be at most 2 dots in word for search queries.
At most 10^4 calls will be made to addWord and search.
*/
const ALPHABET_SIZE: usize = (b'z' - b'a' + 1) as usize;

#[derive(Default)]
struct TreeNode {
    children: [Option<Box<TreeNode>>; ALPHABET_SIZE],
    last_char: bool,
}

#[derive(Default)]
struct WordDictionary {
    root: TreeNode,
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl WordDictionary {
    fn new() -> Self {
        Default::default()
    }

    fn add_word(&mut self, word: String) {
        let mut curr = &mut self.root;
        for c in word.chars() {
            let i = (c as u8 - b'a') as usize;
            if curr.children[i].is_none() {
                curr.children[i] = Some(Box::new(TreeNode::default()));
            }
            curr = curr.children[i].as_deref_mut().unwrap();
        }
        curr.last_char = true;
    }

    fn search(&self, word: String) -> bool {
        self.search_impl(&self.root, &word, 0)
    }

    fn search_impl(&self, n: &TreeNode, word: &String, pos: usize) -> bool {
        const DOT: char = '.';
        if pos >= word.len() {
            return n.last_char;
        }
        let c = word.chars().nth(pos).unwrap();
        let j = (c as u8 - b'a') as usize;
        match c {
            DOT => {
                for child in n.children.iter() {
                    if let Some(ref n) = child {
                        if self.search_impl(n, word, pos + 1) {
                            return true;
                        }
                    }
                }
            }
            _ => match n.children[j] {
                Some(ref n) => {
                    if self.search_impl(n, word, pos + 1) {
                        return true;
                    }
                }
                None => return false,
            },
        }
        false
    }
}

/**
 * Your WordDictionary object will be instantiated and called as such:
 * let obj = WordDictionary::new();
 * obj.add_word(word);
 * let ret_2: bool = obj.search(word);
 */

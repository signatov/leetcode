/*
We are given two strings, A and B.

A shift on A consists of taking string A and moving the leftmost character to the rightmost position.
For example, if A = 'abcde', then it will be 'bcdea' after one shift on A.
Return True if and only if A can become B after some number of shifts on A.
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  bool rotateString(string A, string B) {
    if (A == B) {
      return true;
    }
    if (A.empty() ^ B.empty()) {
      return false;
    }
    bool res = false;
    string original = A;
    do {
      char ch = A[0];
      A = A.erase(0, 1) + ch;
      if (A == B) {
        res = true;
        break;
      }
    } while (original != A); 
    return res;
  }
};

int main() {
  Solution sol;
  cout << sol.rotateString("abcde", "cdeab") << endl;
  cout << sol.rotateString("abcde", "abced") << endl;
  cout << sol.rotateString("", "a") << endl;
}


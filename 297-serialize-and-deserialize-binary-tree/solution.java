/*
https://leetcode.com/problems/serialize-and-deserialize-binary-tree/

297. Serialize and Deserialize Binary Tree

Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.

Clarification: The input/output format is the same as how LeetCode serializes a binary tree. You do not necessarily need to follow this format, so please be creative and come up with different approaches yourself.

Example 1:

Input: root = [1,2,3,null,null,4,5]
Output: [1,2,3,null,null,4,5]

Example 2:

Input: root = []
Output: []
 

Constraints:

The number of nodes in the tree is in the range [0, 10^4].
-1000 <= Node.val <= 1000
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) return null;

        var st = new Stack<TreeNode>();
        var elems = new ArrayList<String>();
        st.push(root);

        while (!st.isEmpty()) {
            var n = st.pop();
            if (n == null) {
                elems.add("null");
            } else {
                elems.add(Integer.toString(n.val));
                st.push(n.right);
                st.push(n.left);
            }
        }

        return "[%s]".formatted(String.join(",", elems));
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data == null) return null;

        assert data.charAt(0) == '[';
        assert data.charAt(data.length() - 1) == ']';

        var s = data.substring(1, data.length() - 1);
        var parts = s.split(",");

        return deserializeImpl(new DeserializationHolder(parts));
    }

    private static TreeNode deserializeImpl(DeserializationHolder holder) {
        var p = holder.token();
        if ("null".equals(p)) return null;

        var n = new TreeNode();
        n.val = Integer.parseInt(p);
        n.left = deserializeImpl(holder.next());
        n.right = deserializeImpl(holder.next());
        return n;
    }


    private static class DeserializationHolder {
        String[] parts;
        int index;
        
        DeserializationHolder(String[] parts) {
            this.parts = parts;
            this.index = 0;
        }

        String token() {
            return parts[index];
        }

        DeserializationHolder next() {
            index++;
            return this;
        }
    }
}

// Your Codec object will be instantiated and called as such:
// Codec ser = new Codec();
// Codec deser = new Codec();
// TreeNode ans = deser.deserialize(ser.serialize(root));


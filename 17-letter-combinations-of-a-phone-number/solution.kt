/*
https://leetcode.com/problems/letter-combinations-of-a-phone-number/

17. Letter Combinations of a Phone Number

Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could
represent. Return the answer in any order.

A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any
letters.

Example 1:

Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

Example 2:

Input: digits = ""
Output: []

Example 3:

Input: digits = "2"
Output: ["a","b","c"]

Constraints:

0 <= digits.length <= 4
digits[i] is a digit in the range ['2', '9'].
*/
class Solution {
    private val mapping = arrayOf(
        arrayOf(""),
        arrayOf(""),
        arrayOf("a", "b", "c"),
        arrayOf("d", "e", "f"),
        arrayOf("g", "h", "i"),
        arrayOf("j", "k", "l"),
        arrayOf("m", "n", "o"),
        arrayOf("p", "q", "r", "s"),
        arrayOf("t", "u", "v"),
        arrayOf("x", "w", "y", "z")
    )

    fun letterCombinations(digits: String): List<String> {
        if (digits == "") return listOf()
        val s = digits.padStart(4, '0')
            .map { it - '0' }
        val res = mutableListOf<String>()
        for (i in mapping[s[0]]) {
            for (j in mapping[s[1]]) {
                for (k in mapping[s[2]]) {
                    for (l in mapping[s[3]]) {
                        res.add("$i$j$k$l")
                    }
                }
            }
        }
        return res
    }
}

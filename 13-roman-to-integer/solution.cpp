/*
https://leetcode.com/problems/roman-to-integer

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

For example, two is written as II in Roman numeral, just two one's added together. 
Twelve is written as, XII, which is simply X + II. The number twenty seven is written as XXVII,
which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for
four is not IIII. Instead, the number four is written as IV. Because the one is before the five we
subtract it making four. The same principle applies to the number nine, which is written as IX.
There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.

Given a roman numeral, convert it to an integer. Input is guaranteed to be within the range from 1 to 3999.

Example 1:

Input: "III"
Output: 3

Example 2:
Input: "IV"
Output: 4

Example 3:
Input: "IX"
Output: 9
Example 4:

Input: "LVIII"
Output: 58
Explanation: C = 100, L = 50, XXX = 30 and III = 3.

Example 5:
Input: "MCMXCIV"
Output: 1994
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
*/
#include <cassert>
#include <iostream>
#include <string>

using namespace std;

class Solution {
  static int parseDigit(char ch) {
    switch (ch) {
      case 'I': return 1;
      case 'V': return 5;
      case 'X': return 10;
      case 'L': return 50;
      case 'C': return 100;
      case 'D': return 500;
      case 'M': return 1000;
      default: break;
    }
    assert(!"Unknown roman digit");
  }
public:
  int romanToInt(string s) {
    int res = 0;
    if (s.empty()) return res;
    int prev = parseDigit(s[0]);
    for (int i = 1; i < s.size(); ++i) {
      int curr = parseDigit(s[i]);
      if (prev == 1 && (curr == 5 || curr == 10))
        res += curr - 1, prev = 0;
      else if (prev == 10 && (curr == 50 || curr == 100))
        res += curr - 10, prev = 0;
      else if (prev == 100 && (curr == 500 || curr == 1000))
        res += curr - 100, prev = 0;
      else {
        if (curr != 1 && curr != 10 && curr != 100) {
          res += curr + prev, prev = 0;
        } else {
          res += prev, prev = curr;
        }
      }
    }
    res += prev;
    return res;
  }
};

int main() {
  Solution sol;
  assert(sol.romanToInt("III") == 3);
  assert(sol.romanToInt("IV") == 4);
  assert(sol.romanToInt("IX") == 9);
  assert(sol.romanToInt("LVIII") == 58);
  assert(sol.romanToInt("MCMXCIV") == 1994);
  assert(sol.romanToInt("CCXLVI") == 246);
  assert(sol.romanToInt("CCVII") == 207);
  assert(sol.romanToInt("MLXVI") == 1066);
  assert(sol.romanToInt("MCMXC") == 1990);
  assert(sol.romanToInt("MDCCLXXVI") == 1776);
}


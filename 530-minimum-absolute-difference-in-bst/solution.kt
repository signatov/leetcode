/*
https://leetcode.com/problems/minimum-absolute-difference-in-bst/

530. Minimum Absolute Difference in BST

Given the root of a Binary Search Tree (BST), return the minimum absolute difference between the values of any two
different nodes in the tree.

Example 1:

Input: root = [4,2,6,1,3]
Output: 1

Example 2:

Input: root = [1,0,48,null,null,12,49]
Output: 1

Constraints:

The number of nodes in the tree is in the range [2, 10^4].
0 <= Node.val <= 10^5
*/
/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class Solution {
    fun getMinimumDifference(root: TreeNode?): Int {
        if (root == null) return 0
        var distance = Int.MAX_VALUE
        fun walk(n: TreeNode) {
            val v = n.`val`
            fun update(u: Int) {
                distance = min(distance, abs(u - v))
            }
            var p = n.left
            while (p != null) {
                update(p.`val`)
                p = p.right
            }
            p = n.right
            while (p != null) {
                update(p.`val`)
                p = p.left
            }
            if (n.left != null) walk(n.left!!)
            if (n.right != null) walk(n.right!!)
        }
        walk(root)
        return distance
    }
}

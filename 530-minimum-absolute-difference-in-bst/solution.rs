/*
https://leetcode.com/problems/minimum-absolute-difference-in-bst/

530. Minimum Absolute Difference in BST

Given the root of a Binary Search Tree (BST), return the minimum absolute difference between the values of any two
different nodes in the tree.

Example 1:

Input: root = [4,2,6,1,3]
Output: 1

Example 2:

Input: root = [1,0,48,null,null,12,49]
Output: 1

Constraints:

The number of nodes in the tree is in the range [2, 10^4].
0 <= Node.val <= 10^5
*/
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::cmp;
use std::rc::Rc;
use std::cell::RefCell;

type MaybeNode = Option<Rc<RefCell<TreeNode>>>;

impl Solution {
  pub fn get_minimum_difference(root: MaybeNode) -> i32 {
    let mut res = i32::MAX;
    let mut prev = None;
    Self::inorder(root, &mut res, &mut prev);
    res
  }

  fn inorder(node: MaybeNode, res: &mut i32, prev: &mut Option<i32>) {
    if let Some(node) = node {
      let node = node.as_ref().borrow();

      Self::inorder(node.left.clone(), res, prev);
      let v = node.val;
      if let Some(prev_value) = *prev {
        *res = cmp::min(*res, v - prev_value);
      }
      *prev = Some(v);
      Self::inorder(node.right.clone(), res, prev);
    }
  }
}

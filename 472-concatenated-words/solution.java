/*
https://leetcode.com/problems/concatenated-words/

472. Concatenated Words

Given an array of strings words (without duplicates), return all the concatenated words in the given list of words.

A concatenated word is defined as a string that is comprised entirely of at least two shorter words (not necessarily distinct) in the given array.

Example 1:

Input: words = ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
Output: ["catsdogcats","dogcatsdog","ratcatdogcat"]
Explanation: "catsdogcats" can be concatenated by "cats", "dog" and "cats";
"dogcatsdog" can be concatenated by "dog", "cats" and "dog";
"ratcatdogcat" can be concatenated by "rat", "cat", "dog" and "cat".

Example 2:

Input: words = ["cat","dog","catdog"]
Output: ["catdog"]

Constraints:
1 <= words.length <= 10^4
1 <= words[i].length <= 30
words[i] consists of only lowercase English letters.
All the strings of words are unique.
1 <= sum(words[i].length) <= 10^5
*/
class Solution {
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        var context = new Context();
        Arrays.stream(words).forEach(context::add);
        return Arrays.stream(words)
            .filter(context::search)
            .toList();
    }

    private static class Node {
        boolean last;
        Node[] next;

        Node() {
            this.last = false;
            this.next = new Node['z' - 'a' + 1];  // only lowercase English letters
        }

        Node next(char c) {
            return this.next[c - 'a'];
        }
    }

    record Context(Node root, Map<String, Boolean> cache) {
        Context() {
            // https://dzone.com/articles/avoid-recursive-invocation-on-computeifabsent-in-h
            this(new Node(), new ConcurrentSkipListMap<>());
        }

        void add(String word) {
            var current = root;
            var chars = word.toCharArray();
            for (var c : chars) {
                int index = c - 'a';
                current = current.next[index] == null
                    ? current.next[index] = new Node() : current.next[index];
            }
            current.last = true;
        }

        boolean search(String word) {
            return search(0, 0, word);
        }

        private boolean searchCached(int index, int count, String word) {
            var key = "%d:%d:%s".formatted(index, count, word);
            return cache.computeIfAbsent(key, k -> search(index, count, word));
        }


        private boolean search(int index, int count, String word) {
            int chars = word.length();
            if (index == chars) return count > 1;
            var current = root;
            while (index < chars && current.next(word.charAt(index)) != null) {
                current = current.next(word.charAt(index++));
                if (current.last) {
                    if (searchCached(index, count + 1, word)) return true;
                }
            }
            return false;
        }
    }
}


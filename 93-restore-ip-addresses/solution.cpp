/*
https://leetcode.com/problems/restore-ip-addresses
Given a string containing only digits, restore it by returning all possible valid IP address combinations.

Example:
Input: "25525511135"
Output: ["255.255.11.135", "255.255.111.35"]
*/
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
  void solver(string s, int index, string address, vector<string> & res) {
    if (index == 4) {
      if (s.empty())
        res.push_back(address.substr(0, address.size() - 1)); // skip last dot.
      return;
    }

    for (int i = 1; i < 4; ++i) {
      if (i <= s.size()) {
        if (i >= 2 && s[0] == '0') continue;
        string part = s.substr(0, i);
        if (i == 3 && atoi(part.c_str()) > 255) continue;
        solver(s.substr(i), index + 1, address + part + ".", res);
      }
    }
  }

public:
  vector<string> restoreIpAddresses(string s) {
    vector<string> res;
    solver(s, 0, "", res);
    return res;
  }
};

int main() {
  Solution sol;
  //const vector<string> &v = sol.restoreIpAddresses("25525511135");
  const vector<string> &v = sol.restoreIpAddresses("010010");
  for (auto i : v) { cout << i << endl; }
}


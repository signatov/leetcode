/*
https://leetcode.com/problems/average-of-levels-in-binary-tree/

637. Average of Levels in Binary Tree

Given the root of a binary tree, return the average value of the nodes on each level in the form of an array. Answers
within 10^-5 of the actual answer will be accepted.

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [3.00000,14.50000,11.00000]
Explanation: The average value of nodes on level 0 is 3, on level 1 is 14.5, and on level 2 is 11.
Hence return [3, 14.5, 11].

Example 2:

Input: root = [3,9,20,15,7]
Output: [3.00000,14.50000,11.00000]

Constraints:

The number of nodes in the tree is in the range [1, 10^4].
-2^31 <= Node.val <= 2^31 - 1
*/
/**
 * Example:
 * var ti = TreeNode(5)
 * var v = ti.`val`
 * Definition for a binary tree node.
 * class TreeNode(var `val`: Int) {
 *     var left: TreeNode? = null
 *     var right: TreeNode? = null
 * }
 */
class Solution {
    fun averageOfLevels(root: TreeNode?): DoubleArray {
        val context = Context()
        averageImpl(root, context, 0)
        return context.toArray()
    }

    private fun averageImpl(root: TreeNode?, context: Context, level: Int) {
        if (root == null) return
        val counter = context.get(level)
        counter.add(root.`val`)
        averageImpl(root.left, context, level + 1)
        averageImpl(root.right, context, level + 1)
    }

    class Context {
        private val list = ArrayList<Counter>()

        fun get(i: Int): Counter {
            return when {
                i < list.size -> list[i]
                else -> {
                    val c = Counter()
                    list.add(c)
                    c
                }
            }
        }

        fun toArray(): DoubleArray = DoubleArray(list.size) { list[it].avg() }
    }

    class Counter {
        private var sum: Long = 0L
        private var count: Int = 0

        fun add(value: Int) {
            sum += value.toLong()
            ++count
        }

        fun avg(): Double = sum.toDouble() / count
    }
}

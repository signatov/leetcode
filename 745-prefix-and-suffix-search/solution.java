/*
https://leetcode.com/problems/prefix-and-suffix-search/

745. Prefix and Suffix Search

Design a special dictionary that searches the words in it by a prefix and a suffix.

Implement the WordFilter class:

WordFilter(string[] words) Initializes the object with the words in the dictionary.
f(string pref, string suff) Returns the index of the word in the dictionary, which has the prefix pref and the suffix suff. If there is more than one valid index, return the largest of them. If there is no such word in the dictionary, return -1.

Example 1:

Input
["WordFilter", "f"]
[[["apple"]], ["a", "e"]]
Output
[null, 0]
Explanation
WordFilter wordFilter = new WordFilter(["apple"]);
wordFilter.f("a", "e"); // return 0, because the word at index 0 has prefix = "a" and suffix = "e".

Constraints:
1 <= words.length <= 10^4
1 <= words[i].length <= 7
1 <= pref.length, suff.length <= 7
words[i], pref and suff consist of lowercase English letters only.
At most 10^4 calls will be made to the function f.
*/
class WordFilter {
    private static final int MAX = 7;

    private final Map<String, Integer> indexes = new HashMap<>();
    private final Set<String> processed = new HashSet<>();

    public WordFilter(String[] words) {
        for (int i = words.length - 1; i >= 0; --i) {
            var word = words[i];
            if (processed.contains(word)) continue;
            processed.add(word);
            int l = Math.min(MAX, word.length());
            for (int j = 0; j < l; ++j) {
                var prefix = word.substring(0, j + 1);
                for (int k = word.length() - 1; k >= Math.max(word.length() - MAX, 0); --k) {
                    var suffix = word.substring(k);
                    var key = makeKey(prefix, suffix);
                    indexes.put(key, Math.max(i, indexes.getOrDefault(key, -1)));
                }
            }
        }
    }

    public int f(String pref, String suff) {
        var key = makeKey(pref, suff);
        return indexes.getOrDefault(key, -1);
    }

    private static String makeKey(String prefix, String suffix) {
        return String.join("\0", prefix, suffix);
    }
}

/**
 * Your WordFilter object will be instantiated and called as such:
 * WordFilter obj = new WordFilter(words);
 * int param_1 = obj.f(pref,suff);
 */


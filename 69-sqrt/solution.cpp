/*
https://leetcode.com/problems/sqrtx

Implement int sqrt(int x).

Compute and return the square root of x.
*/
#include <iostream>
#include <cstdint>

using namespace std;

class Solution {
public:
  int mySqrt(int x) {
    if (x <= 0) return 0;
    unsigned int l = 1, r = x / 2;
    while (l < r) {
      uint64_t l2 = l * l, r2 = r * r;
      if (l2 == x) return l;
      if (r2 == x) return r;
      if (r - l == 1 && l2 < x && r2 > x) return l;
      if (r - l == 1 && l2 < x && r2 < x) { r++; continue; }

      if (l2 < x) { l = (l + r) / 2; continue; }
      if (l2 > x) { r = l, l = r / 2; continue; }

      if (r2 > x) { r = (l + r) / 2; continue; }
      if (r2 < x) { l = r, r = x / 2; continue; }
    }
    return l;
  }
};

int main() {
  Solution sol;
  cout << sol.mySqrt(44) << endl;
  cout << sol.mySqrt(9) << endl;
  cout << sol.mySqrt(12) << endl;
  cout << sol.mySqrt(4) << endl;
  cout << sol.mySqrt(8) << endl;
  cout << sol.mySqrt(5) << endl;
  cout << sol.mySqrt(1) << endl;
  cout << sol.mySqrt(2) << endl;
  cout << sol.mySqrt(2147395599) << endl;
}

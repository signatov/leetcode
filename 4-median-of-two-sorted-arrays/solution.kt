/*
https://leetcode.com/problems/median-of-two-sorted-arrays/

4. Median of Two Sorted Arrays

Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

The overall run time complexity should be O(log (m+n)).

Example 1:

Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.

Example 2:

Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

Constraints:

nums1.length == m
nums2.length == n
0 <= m <= 1000
0 <= n <= 1000
1 <= m + n <= 2000
-10^6 <= nums1[i], nums2[i] <= 10^6
*/
class Solution {
    fun findMedianSortedArrays(nums1: IntArray, nums2: IntArray): Double {
        val a = MergeArray(nums1, nums2)
        return when (a.size) {
            1 -> 1.0 * a[0]
            2 -> (a[0] + a[1]) / 2.0
            else -> {
                val middle = a.size / 2
                if (a.size % 2 == 0) (a[middle] + a[middle - 1]) / 2.0 else 1.0 * a[middle]
            }
        }
    }

    class MergeArray(private val nums1: IntArray, private val nums2: IntArray) {
        val size: Int = nums1.size + nums2.size

        operator fun get(i: Int): Int {
            var j = 0
            var k = 0
            var l = 0
            var r = 0
            while (j <= i) {
                r = when {
                    k < nums1.size && l < nums2.size ->
                        if (nums1[k] <= nums2[l]) nums1[k++] else nums2[l++]
                    k >= nums1.size -> nums2[l++]
                    else -> nums1[k++]
                }
                ++j
            }
            return r
        }
    }
}

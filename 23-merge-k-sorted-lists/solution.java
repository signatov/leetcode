/*
https://leetcode.com/problems/merge-k-sorted-lists/

23. Merge k Sorted Lists

You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.

Merge all the linked-lists into one sorted linked-list and return it.

Example 1:

Input: lists = [[1,4,5],[1,3,4],[2,6]]
Output: [1,1,2,3,4,4,5,6]
Explanation: The linked-lists are:
[
  1->4->5,
  1->3->4,
  2->6
]
merging them into one sorted list:
1->1->2->3->4->4->5->6

Example 2:

Input: lists = []
Output: []

Example 3:

Input: lists = [[]]
Output: []
 

Constraints:

k == lists.length
0 <= k <= 10^4
0 <= lists[i].length <= 500
-10^4 <= lists[i][j] <= 10^4
lists[i] is sorted in ascending order.
The sum of lists[i].length will not exceed 10^4.
*/
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists.length == 0) return null;
        var r = findMin(lists);
        var l = r;
        boolean found = false;
        do {
            var n = findMin(lists);
            found = n != null;
            if (l != null) l.next = n;
            l = n;
        } while (found);
        return r;
    }

    private static ListNode findMin(ListNode[] lists) {
        int pos = -1;
        var min = Integer.MAX_VALUE;
        for (int i = 0; i < lists.length; ++i) {
            if (lists[i] == null) continue;
            var c = lists[i].val;
            if (min > c) {
                min = c;
                pos = i;
            }
        }
        if (pos < 0) return null;
        var l = lists[pos];
        lists[pos] = l != null ? l.next : null;
        return new ListNode(min);
    }
}


/*
https://leetcode.com/problems/rotate-array/

189. Rotate Array

Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.

Example 1:

Input: nums = [1,2,3,4,5,6,7], k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]

Example 2:

Input: nums = [-1,-100,3,99], k = 2
Output: [3,99,-1,-100]
Explanation:
rotate 1 steps to the right: [99,-1,-100,3]
rotate 2 steps to the right: [3,99,-1,-100]

Constraints:

1 <= nums.length <= 10^5
-2^31 <= nums[i] <= 23^1 - 1
0 <= k <= 10^5
*/
class Solution {
    fun rotate(nums: IntArray, k: Int) {
        var t = k % nums.size
        if (t == 0) return
        while (t-- > 0)
            rotate1(nums)
    }

    private fun rotate1(nums: IntArray) {
        val t = nums.last()
        System.arraycopy(nums, 0, nums, 1, nums.size - 1)
        nums[0] = t
    }
}


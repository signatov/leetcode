/*
https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/

103. Binary Tree Zigzag Level Order Traversal

Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to
right, then right to left for the next level and alternate between).

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [[3],[20,9],[15,7]]

Example 2:

Input: root = [1]
Output: [[1]]

Example 3:

Input: root = []
Output: []

Constraints:

The number of nodes in the tree is in the range [0, 2000].
-100 <= Node.val <= 100
*/
// Definition for a binary tree node.
// #[derive(Debug, PartialEq, Eq)]
// pub struct TreeNode {
//   pub val: i32,
//   pub left: Option<Rc<RefCell<TreeNode>>>,
//   pub right: Option<Rc<RefCell<TreeNode>>>,
// }
//
// impl TreeNode {
//   #[inline]
//   pub fn new(val: i32) -> Self {
//     TreeNode {
//       val,
//       left: None,
//       right: None
//     }
//   }
// }
use std::rc::Rc;
use std::cell::RefCell;
impl Solution {
  pub fn zigzag_level_order(root: Option<Rc<RefCell<TreeNode>>>) -> Vec<Vec<i32>> {
    if root.is_none() { return vec![]; }
    let mut res = Vec::new();
    fn travelsal(node: Option<Rc<RefCell<TreeNode>>>, level: usize, v: &mut Vec<Vec<i32>>) {
      if let Some(node) = node {
        let node = node.as_ref().borrow();
        if v.len() < level + 1 { v.push(Vec::new()); }
        if level % 2 == 0 {
          v[level].push(node.val);
        } else {
          v[level].insert(0, node.val);
        }
        travelsal(node.left.clone(), level + 1, v);
        travelsal(node.right.clone(), level + 1, v);
      }
    }
    travelsal(root, 0, &mut res);
    res
  }
}

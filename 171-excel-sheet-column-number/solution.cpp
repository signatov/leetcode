/*
Given a column title as appear in an Excel sheet, return its corresponding column number.
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  int titleToNumber(string s) {
    if (s.empty()) return 0;
    int res = 0;
    int p = 1;
    for (auto i = s.rbegin(); i != s.rend(); ++i, p *= 'Z' - 'A' + 1) {
      int d = (*i) - 'A' + 1;
      res += d * p;
    }
    return res;
  }
};

int main() {
  Solution sol;
  cout << sol.titleToNumber("A") << endl;
  cout << sol.titleToNumber("C") << endl;
  cout << sol.titleToNumber("Z") << endl;
  cout << sol.titleToNumber("AA") << endl;
  cout << sol.titleToNumber("AB") << endl;
  cout << sol.titleToNumber("AAA") << endl;
}

/*
Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

For example,
"A man, a plan, a canal: Panama" is a palindrome.
"race a car" is not a palindrome.
*/
// c++ --std=c++14 solution.cpp
#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <functional>

using namespace std;

class Solution {
public:
  bool isPalindrome(string s) {
    if (s.empty()) return true;
    string v = s;
    transform(v.begin(), v.end(), v.begin(), ::tolower);
    v.erase(remove_if(v.begin(), v.end(), [](const auto &c) -> bool { return !isalnum(c); }), v.end());
    if (v.size() == 0) return true;
    for (int i = 0, size = v.size(); i < v.size() / 2; ++i) {
      if (v[i] != v[size - i - 1])
        return false;
    }
    return true;
  }
};

int main() {
  Solution sol;
  cout << sol.isPalindrome("A man, a plan, a canal: Panama") << endl;
  cout << sol.isPalindrome("race a car") << endl;
  cout << sol.isPalindrome("a") << endl;
}

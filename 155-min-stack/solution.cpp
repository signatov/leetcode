/*
https://leetcode.com/problems/min-stack

Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.
Example:
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin();   --> Returns -3.
minStack.pop();
minStack.top();      --> Returns 0.
minStack.getMin();   --> Returns -2.
*/
#include <cassert>
#include <climits>
#include <cstring>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

class StackBase {
protected:
  const int block_size = 16 * 1024;
  int *pool;
  int *head;
  int *tail;
  int used = 0;
  int size = 0;

  void display() {
    for (int *p = head; p < tail; ++p) {
      cout << *p << (p < tail - 1 ? "," : "");
    }
    cout << endl;
  }
  void debug(const char * prefix) {
    cout << prefix << ": head=" << (intptr_t)head
                   << " tail=" << (intptr_t)tail
                   << " size=" << size
                   << " used=" << used
                   << " dt=" << (intptr_t)tail - (intptr_t)head
                   << endl;
  }

  void realloc() {
    size_t dt = (intptr_t)tail - (intptr_t)head;
    used *= 2;
    int *t = (int *)malloc(used);
    if (!t)
      throw bad_alloc();
    memcpy(t, head, dt);
    free(pool);
    pool = head = t;
    tail = (int *)((intptr_t)t + size * sizeof(int));
  }

public:
  /** initialize your data structure here. */
  StackBase() {
    pool = head = tail = (int *)malloc(used = block_size);
    if (!pool)
      throw bad_alloc();
  }

  ~StackBase() {
    free(pool);
  }

  void push(int x) {
    size_t dt = (intptr_t)tail - (intptr_t)head;
    if (dt >= used) realloc();
    *tail++ = x;
    ++size;
  }

  void pop() {
    if (size == 0)
      throw length_error("Stack is empty (pop operation)");
    --tail;
    if (--size == 0) {
      assert(head == tail);
      head = tail = pool;
    }
  }

  int top() {
    if (size == 0)
      throw length_error("Stack is empty (top operation)");
    return *(tail - 1);
  }

  bool empty() const { return size == 0; }
};

class MinStack : public StackBase {
  typedef StackBase Super;
  StackBase mins;
public:
  /** initialize your data structure here. */
  MinStack() : Super() {}

  void push(int x) {
    Super::push(x);
    if (!mins.empty()) {
      if (x <= getMin()) {
        mins.push(x);
      }
    } else {
      mins.push(x);
    }
  }

  void pop() {
    int v = top();
    Super::pop();
    if (v == getMin()) {
      mins.pop();
    }
  }

  int getMin() { return mins.top(); }
};

void check1() {
  MinStack stack;
  stack.push(-1);
  assert(stack.getMin() == -1);
  stack.push(-2);
  assert(stack.getMin() == -2);
  for (int i = 0; i < 4096; ++i) {
    stack.push(i);
  }
  assert(stack.top() == 4095);
  stack.pop();
  assert(stack.top() == 4094);
  stack.pop();
  assert(stack.top() == 4093);
}

void check2() {
  MinStack stack;
  stack.push(1);
  stack.push(2);
  stack.push(3);
  stack.pop();
  stack.pop();
  stack.pop();
  stack.push(666);
  assert(stack.top() == 666);
}

void check3() {
  MinStack stack;
  stack.push(1);
  stack.pop();
  stack.push(2);
  stack.push(3);
  stack.push(4);
  stack.push(5);
}

void check4() {
  MinStack stack;
  stack.push(-2);
  stack.push(-1);
  assert(stack.getMin() == -2);
  stack.pop();
  assert(stack.getMin() == -2);
  stack.push(-3);
  assert(stack.getMin() == -3);
  stack.pop();
  assert(stack.getMin() == -2);
  stack.push(-3);
  stack.push(-2);
  assert(stack.getMin() == -3);
  stack.pop();
  assert(stack.getMin() == -3); 
}

void check5() {
  MinStack stack;
  stack.push(-2);
  stack.push(0);
  stack.push(-3);
  assert(stack.getMin() == -3);
  stack.pop();
  assert(stack.top() == 0);
  assert(stack.getMin() == -2);
}

void check6() {
  MinStack stack;
  stack.push(0);
  stack.push(1);
  stack.push(0);
  assert(stack.getMin() == 0);
  stack.pop();
  assert(stack.getMin() == 0);
}

int main() {
  check1();
  check2();
  check3();
  check4();
  check5();
  check6();
}


/*
You're given strings J representing the types of stones that are jewels, and S representing the stones you have.
Each character in S is a type of stone you have. You want to know how many of the stones you have are also jewels.

The letters in J are guaranteed distinct, and all characters in J and S are letters.
Letters are case sensitive, so "a" is considered a different type of stone from "A".
*/
#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
  int numJewelsInStones(string J, string S) {
    int res = 0;
    for (auto ch : J) {
      for (;;) {
        size_t pos = S.find(ch);
        if (pos != string::npos) {
          ++res;
          S = S.erase(pos, 1);
        } else { break; }
      }
    }
    return res;
  }
};

int main() {
  Solution sol;
  cout << sol.numJewelsInStones("aA", "aAAbbbb") << endl;
  cout << sol.numJewelsInStones("z", "ZZ") << endl;
}

/*
https://leetcode.com/problems/palindrome-number

Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:
Input: 121
Output: true

Example 2:
Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

Example 3:
Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Follow up:
Coud you solve it without converting the integer to a string?
*/
#include <cassert>

struct Solution {
  bool isPalindrome(int x) {
    if (x < 0) return false;
    if (x == 0) return true;
    if (x % 10 == 0) return false;
    return x == reverse(x);
  }

private:
  static long long reverse(int x) {
    if (x == 0) return 0;
    long long r{0LL};
    while (x > 0) {
      int t = x % 10;
      r = r * 10 + t;
      x /= 10;
    }
    return r;
  }
};

int main() {
  Solution sol;
  assert(sol.isPalindrome(0));
  assert(sol.isPalindrome(121));
  assert(!sol.isPalindrome(123));
  assert(!sol.isPalindrome(-121));
  assert(!sol.isPalindrome(10));
  assert(!sol.isPalindrome(2147483647));
}


/*
https://leetcode.com/problems/maximal-square/

221. Maximal Square

Given an m x n binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

Example 1:

Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
Output: 4

Example 2:

Input: matrix = [["0","1"],["1","0"]]
Output: 1

Example 3:

Input: matrix = [["0"]]
Output: 0

Constraints:

m == matrix.length
n == matrix[i].length
1 <= m, n <= 300
matrix[i][j] is '0' or '1'.
*/
class Solution {
    fun maximalSquare(matrix: Array<CharArray>): Int {
        val rows = matrix.size
        val cols = matrix[0].size
        var prevRow = IntArray(cols) { 0 }
        var currRow = IntArray(cols) { 0 }
        var res = 0
        for (i in 0 until rows) {
            for (j in 0 until cols) {
                if (matrix[i][j] == '0') {
                    currRow[j] = 0
                    continue
                }
                val t = when {
                    i == 0 || j == 0 -> 1
                    else -> 1 + minOf(prevRow[j - 1], prevRow[j], currRow[j - 1])
                }
                res = maxOf(res, t)
                currRow[j] = t
            }
            prevRow = currRow.also { currRow = prevRow }
        }
        return res * res
    }
}

/*
https://leetcode.com/problems/lru-cache

Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.

Implement the LRUCache class:

LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
int get(int key) Return the value of the key if the key exists, otherwise return -1.
void put(int key, int value) Update the value of the key if the key exists. Otherwise, add the key-value pair to the
cache. If the number of keys exceeds the capacity from this operation, evict the least recently used key.
The functions get and put must each run in O(1) average time complexity.

Example 1:

Input
["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
Output
[null, null, null, 1, null, -1, null, -1, 3, 4]

Explanation
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // cache is {1=1}
lRUCache.put(2, 2); // cache is {1=1, 2=2}
lRUCache.get(1);    // return 1
lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
lRUCache.get(2);    // returns -1 (not found)
lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
lRUCache.get(1);    // return -1 (not found)
lRUCache.get(3);    // return 3
lRUCache.get(4);    // return 4

Constraints:

1 <= capacity <= 3000
0 <= key <= 10^4
0 <= value <= 10^5
At most 2 * 10^5 calls will be made to get and put.
*/
use std::collections::{HashMap, LinkedList};

#[derive(Default)]
struct LRUCache {
  data: HashMap<i32, i32>,
  counters: HashMap<i32, i32>,
  queue: LinkedList<i32>,
  capacity: usize
}

/**
 * `&self` means the method takes an immutable reference.
 * If you need a mutable reference, change it to `&mut self` instead.
 */
impl LRUCache {
  fn new(capacity: i32) -> Self {
    Self {
      capacity: capacity as _,
      ..Default::default()
    }
  }

  fn get(&mut self, key: i32) -> i32 {
    if let Some(&v) = self.data.get(&key) {
      self.use_key(key);
      v
    } else {
      -1
    }
  }

  fn put(&mut self, key: i32, value: i32) {
    if self.data.len() == self.capacity && !self.data.contains_key(&key) {
      self.evict();
    }
    self.data.insert(key, value);
    self.use_key(key);
  }

  fn evict(&mut self) {
    while let Some(v) = self.queue.pop_front() {
      if *self.counters.entry(v).and_modify(|c| *c -= 1).or_default() == 0 {
        self.counters.remove(&v);
        self.data.remove(&v);
        break;
      }
    }
  }

  fn use_key(&mut self, key: i32) {
    self.queue.push_back(key);
    *self.counters.entry(key).or_default() += 1;
  }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * let obj = LRUCache::new(capacity);
 * let ret_1: i32 = obj.get(key);
 * obj.put(key, value);
 */

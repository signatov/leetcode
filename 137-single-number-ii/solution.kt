/*
https://leetcode.com/problems/single-number-ii/

137. Single Number II

Given an integer array nums where every element appears three times except for one, which appears exactly once. Find
the single element and return it.

You must implement a solution with a linear runtime complexity and use only constant extra space.

Example 1:

Input: nums = [2,2,3,2]
Output: 3

Example 2:

Input: nums = [0,1,0,1,0,1,99]
Output: 99

Constraints:

1 <= nums.length <= 3 * 10^4
-2^31 <= nums[i] <= 23^1 - 1
Each element in nums appears exactly three times except for one element which appears once.
*/
class Solution {
    fun singleNumber(nums: IntArray): Int {
        var res = 0
        for (i in 0 until Int.SIZE_BITS) {
            var sum = 0
            for (n in nums)
                sum += (n shr i) and 1
            sum %= 3
            res = res or (sum shl i)
        }
        return res
    }
}

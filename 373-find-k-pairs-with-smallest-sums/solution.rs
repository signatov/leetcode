/*
https://leetcode.com/problems/find-k-pairs-with-smallest-sums

373. Find K Pairs with Smallest Sums

You are given two integer arrays nums1 and nums2 sorted in non-decreasing order and an integer k.

Define a pair (u, v) which consists of one element from the first array and one element from the second array.

Return the k pairs (u1, v1), (u2, v2), ..., (uk, vk) with the smallest sums.

Example 1:

Input: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
Output: [[1,2],[1,4],[1,6]]
Explanation: The first 3 pairs are returned from the sequence: [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]

Example 2:

Input: nums1 = [1,1,2], nums2 = [1,2,3], k = 2
Output: [[1,1],[1,1]]
Explanation: The first 2 pairs are returned from the sequence: [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]

Constraints:

1 <= nums1.length, nums2.length <= 10^5
-10^9 <= nums1[i], nums2[i] <= 10^9
nums1 and nums2 both are sorted in non-decreasing order.
1 <= k <= 10^4
k <= nums1.length * nums2.length
*/
use std::collections::BinaryHeap;

#[derive(Copy, Clone, Eq, PartialEq)]
struct Pair {
    sum: i32,
    i: usize,
    j: usize,
}

impl PartialOrd for Pair {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Pair {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.sum.cmp(&self.sum)
    }
}

impl Solution {
    pub fn k_smallest_pairs(nums1: Vec<i32>, nums2: Vec<i32>, k: i32) -> Vec<Vec<i32>> {
        if k == 1 {
            return vec![vec![nums1[0], nums2[0]]];
        }
        let mut res: Vec<Vec<i32>> = Vec::new();
        let row_size = std::cmp::min(nums1.len(), k as usize);
        let mut heap = BinaryHeap::new();
        for i in 0..row_size {
            heap.push(Pair { sum: nums1[i] + nums2[0], i, j: 0 });
        }
        let mut count = k;
        while count > 0 && !heap.is_empty() {
            let Pair { sum: _, i, j } = heap.pop().unwrap();
            res.push(vec![nums1[i], nums2[j]]);
            if j + 1 < nums2.len() {
                heap.push(Pair { sum: nums1[i] + nums2[j + 1], i, j: j + 1 })
            }
            count -= 1;
        }
        res
    }
}

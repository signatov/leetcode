/*
Write a program that outputs the string representation of numbers from 1 to n.

But for multiples of three it should output “Fizz” instead of the number and for the multiples of five output “Buzz”.
For numbers which are multiples of both three and five output “FizzBuzz”.
*/
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
  vector<string> fizzBuzz(int n) {
    vector<string> res;
    if (n <= 0) { return res; }

    res.reserve(n);
    for (int i = 1; i <= n; ++i) {
      string num;
      if (i % 3 == 0) num += "Fizz";
      if (i % 5 == 0) num += "Buzz";
      if (num.empty()) num = to_string(i);
      res.push_back(num);
    }

    return res;
  }
};

int main() {
  Solution sol;
  vector<string> r = sol.fizzBuzz(15);
  for (auto s : r) { cout << s << endl; }  
}


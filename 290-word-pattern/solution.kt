/*
https://leetcode.com/problems/word-pattern/

290. Word Pattern

Given a pattern and a string s, find if s follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in s.

Example 1:

Input: pattern = "abba", s = "dog cat cat dog"
Output: true

Example 2:

Input: pattern = "abba", s = "dog cat cat fish"
Output: false

Example 3:

Input: pattern = "aaaa", s = "dog cat cat dog"
Output: false

Constraints:

1 <= pattern.length <= 300
pattern contains only lower-case English letters.
1 <= s.length <= 3000
s contains only lowercase English letters and spaces ' '.
s does not contain any leading or trailing spaces.
All the words in s are separated by a single space.
*/
class Solution {
    fun wordPattern(pattern: String, s: String): Boolean {
        val words = s.split(" ")
        if (words.size != pattern.length) return false
        val matches = mutableMapOf<Char, String>()
        val used = mutableSetOf<String>()
        for (i in words.indices) {
            val word = words[i]
            val c = pattern[i]
            if (matches.containsKey(c)) {
                if (word != matches[c]) return false
            } else {
                if (used.contains(word)) return false
                matches[c] = word
                used.add(word)
            }
        }
        return true
    }
}
